<?php
class ExportData_model extends CI_Model {

        public function __construct()
        {

        }

        public function getNamaTable(){
                $query = $this->db->query("SELECT TABLE_NAME AS namatable FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA ='".$this->db->database."'");
                return $query->result_array();
        }

        public function getDataTable($namaTable){
            $query = $this->db->query("select * from ".$namaTable);
            return $query->result_array();
        }

        public function getNameColumn($namaTable){
            $query = $this->db->query("SELECT COLUMN_NAME as namakolom FROM information_schema.columns WHERE table_schema='".$this->db->database."' AND table_name='".$namaTable."'");
                return $query->result_array();
        }
}
?>