<?php
class SubDetailKomoditi_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListSubDetailKomoditi2($kategori,$komoditas){
                $str ="";
                if($kategori != 'ALL'){
                        $str .= "where idKategoriKomoditi =".$kategori;
                }else{
                        $str .= "";
                }

                if($komoditas != 'ALL'){
                        $str .= " and idKomoditi =".$komoditas;
                }else{
                        $str .= "";
                }
                $query = $this->db->query("select concat(`namaKategoriKomoditi`,' - ',`namaKomoditi`,' ',`namaSubDetailKomoditi`) as namasubdetailkomoditi,namaSatuan,idSubDetailKomoditi from `subDetailKomoditi` left join `komoditi` on `komoditiIdKomoditi`=`idKomoditi` left join `kategoriKomoditi` on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on satuanIdSatuan=idSatuan ".$str." order by orderByKategoriKomoditi,namaKomoditi");
                return $query->result_array();
        }

        public function getListSubDetailKomoditi(){
                $query = $this->db->query("select concat(`namaKategoriKomoditi`,' - ',`namaKomoditi`,' ',`namaSubDetailKomoditi`) as namasubdetailkomoditi,namaSatuan,idSubDetailKomoditi from `subDetailKomoditi` left join `komoditi` on `komoditiIdKomoditi`=`idKomoditi` left join `kategoriKomoditi` on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on satuanIdSatuan=idSatuan order by orderByKategoriKomoditi,namaKomoditi");
                return $query->result_array();
        }

        public function inputHargaKomoditi($komoditi,$namasubdetailkomoditi,$satuan)
	{
                $query = $this->db->query("insert into subDetailKomoditi (komoditiIdKomoditi,namaSubDetailKomoditi,satuanIdSatuan) values (".$komoditi.",'".$namasubdetailkomoditi."',".$satuan.")");
                return $query;
	}

        public function getListKomoditi(){
                $query = $this->db->query("select idKategoriKomoditi,namaKategoriKomoditi,GROUP_CONCAT(namaKomoditi separator '|') as namakomoditi,GROUP_CONCAT(idKomoditi separator '|') as idkomoditi from komoditi left join kategoriKomoditi on idKategoriKomoditi=kategoriIdKategoriKomoditi group by idKategoriKomoditi");
                return $query->result_array();
        }

        public function getSubKomoditi($id){
                $query = $this->db->query("select idSubDetailKomoditi,komoditiIdKomoditi,namaSubDetailKomoditi,satuanIdSatuan from subDetailKomoditi where idSubDetailKomoditi=".$id);
                return $query->row();
        }

        public function updateSubKomoditi($komoditi,$namasubdetailkomoditi,$satuan,$idsubkomoditi){
                $query = $this->db->query("update subDetailKomoditi set komoditiIdKomoditi=".$komoditi.",namaSubDetailKomoditi='".$namasubdetailkomoditi."',satuanIdSatuan=".$satuan." where idSubDetailKomoditi=".$idsubkomoditi);
                return $query;
        }

        public function deleteSubKomoditi($id){
                $query = $this->db->query("select count(*) as jml from detailKomoditi where subDetailIdSubDetail =".$id);
                $data = $query->row();
                if($data->jml > 0){
                        return false;
                }else{
                        $query = $this->db->query("delete from subDetailKomoditi where idSubDetailKomoditi=".$id);
                        return $query;
                }
                
        }

        public function getKomoditasByKategori($val){
                $str ="";
                if($val != 'ALL'){
                        $str .= "where kategoriIdKategoriKomoditi=".$val;
                }
                $query = $this->db->query("select * from komoditi ".$str);
                return $query->result_array();
        }
}
?>