<?php
class Frontier_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListLaporan2($id,$kat,$datesearch){
                $str = "";
                $str2 = "";

                if($id != 'ALL'){
                        $str .= " and idKecamatan =".$id;
                }else{
                        $str .= "";
                }

                if($kat != 'ALL'){
                        $str2 .= " where idKategoriKomoditi =".$kat;
                }else{
                        $str2 .= "";
                }

                $query = $this->db->query("select idSubDetailKomoditi,concat(namaKomoditi,' ',namaSubDetailKomoditi) as namakomoditi,concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1,concat('Rp ', format(ifnull(avg(b.harga),0), 0)) as hargaavg2,concat('Rp ', format(ifnull(avg(c.harga),0), 0)) as hargaavg3,concat('Rp ', format(ifnull(avg(d.harga),0), 0)) as hargaavg4,concat('Rp ', format(ifnull(avg(e.harga),0), 0)) as hargaavg5,concat('Rp ', format(ifnull(avg(f.harga),0), 0)) as hargaavg6,concat('Rp ', format(ifnull(avg(g.harga),0), 0)) as hargaavg7,concat('Rp ', format(ifnull(avg(h.harga),0), 0)) as hargaavg8,concat('Rp ', format(ifnull(avg(i.harga),0), 0)) as hargaavg9,concat(ifnull(avg(h.harga),0)-ifnull(avg(i.harga),0),',',ifnull(avg(g.harga),0)-ifnull(avg(h.harga),0),',',',',ifnull(avg(f.harga),0)-ifnull(avg(g.harga),0),',',ifnull(avg(e.harga),0)-ifnull(avg(f.harga),0),',',ifnull(avg(d.harga),0)-ifnull(avg(e.harga),0),',',ifnull(avg(c.harga),0)-ifnull(avg(d.harga),0),',',ifnull(avg(b.harga),0)-ifnull(avg(c.harga),0),',',ifnull(avg(a.harga),0)-ifnull(avg(b.harga),0)) as node1,ifnull(avg(a.harga),0) as hargaawal,namaSatuan,ifnull(avg(a.harga),0) as hargabiasa1,ifnull(avg(b.harga),0) as hargabiasa2 from subDetailKomoditi 
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 0 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 1 DAY) ".$str.") as b on subDetailKomoditi.idSubDetailKomoditi=b.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 2 DAY) ".$str.") as c on subDetailKomoditi.idSubDetailKomoditi=c.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 3 DAY) ".$str.") as d on subDetailKomoditi.idSubDetailKomoditi=d.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 4 DAY) ".$str.") as e on subDetailKomoditi.idSubDetailKomoditi=e.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 5 DAY) ".$str.") as f on subDetailKomoditi.idSubDetailKomoditi=f.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 6 DAY) ".$str.") as g on subDetailKomoditi.idSubDetailKomoditi=g.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$datesearch."', INTERVAL 7 DAY) ".$str.") as h on subDetailKomoditi.idSubDetailKomoditi=h.subDetailIdSubDetail
                left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB(".$datesearch.", INTERVAL 8 DAY) ".$str.") as i on subDetailKomoditi.idSubDetailKomoditi=i.subDetailIdSubDetail
                left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan ".$str2." group by idSubDetailKomoditi");
                return $query->result_array();
        }

        public function getListKecamatan(){
                $query = $this->db->query("select idKecamatan,namaKecamatan from kecamatan order by namaKecamatan asc");
                return $query->result_array();
        }

        // public function getPricesSevenDays($id,$idSubDetailKomoditi,$k){
        //         $str = "";
        //         if($id != 'ALL'){
        //                 $str .= " and idKecamatan =".$id;
        //         }else{
        //                 $str .= "";
        //         }

        //         $query = $this->db->query("select ifnull(avg(a.harga),0) as hargaku from subDetailKomoditi left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('2019-02-28', INTERVAL ".$k." DAY) and subDetailIdSubDetail = ".$idSubDetailKomoditi." ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan where idKategoriKomoditi=2 group by idSubDetailKomoditi");
        //         return $query->result_array();
        // }

        public function getKecamatanRataRata($komId,$idKec,$tanggal){
                $str = "";
                if($idKec != 'ALL'){
                        $str .= " where idKecamatan =".$idKec;
                }else{
                        $str .= "";
                }

                $query = $this->db->query("select idKecamatan,namaKecamatan,ifnull(avg(a.harga),'-') as satu,ifnull(avg(b.harga),'-') as dua,ifnull(avg(c.harga),'-') as tiga,ifnull(avg(d.harga),'-') as empat,ifnull(avg(e.harga),'-') as lima from kecamatan left join pasar on idKecamatan=kecamatanIdKecamatan left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 0 DAY) and subDetailIdSubDetail = ".$komId.") as a on pasar.idPasar=a.pasarIdPasar left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 1 DAY) and subDetailIdSubDetail = ".$komId.") as b on pasar.idPasar=b.pasarIdPasar left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 2 DAY) and subDetailIdSubDetail = ".$komId.") as c on pasar.idPasar=c.pasarIdPasar left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 3 DAY) and subDetailIdSubDetail = ".$komId.") as d on pasar.idPasar=d.pasarIdPasar left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 4 DAY) and subDetailIdSubDetail = ".$komId.") as e on pasar.idPasar=e.pasarIdPasar ".$str." group by idKecamatan");
                return $query->result_array();
        }

        public function getPricePasar($komId,$idKec,$tanggal){
                $query = $this->db->query("select pasar.namaPasar,format(ifnull(a.harga,'-'),0) as satu,format(ifnull(b.harga,'-'),0) as dua,format(ifnull(c.harga,'-'),0) as tiga,format(ifnull(d.harga,'-'),0) as empat,format(ifnull(e.harga,'-'),0) as lima from pasar 
                left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 0 DAY) and subDetailIdSubDetail = ".$komId.") as a on pasar.idPasar=a.pasarIdPasar
                left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 1 DAY) and subDetailIdSubDetail = ".$komId.") as b on pasar.idPasar=b.pasarIdPasar
                left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 2 DAY) and subDetailIdSubDetail = ".$komId.") as c on pasar.idPasar=c.pasarIdPasar
                left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 3 DAY) and subDetailIdSubDetail = ".$komId.") as d on pasar.idPasar=d.pasarIdPasar
                left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal."', INTERVAL 4 DAY) and subDetailIdSubDetail = ".$komId.") as e on pasar.idPasar=e.pasarIdPasar
                where kecamatanIdKecamatan = ".$idKec);
                return $query->result_array();
        }

        // public function getPriceDayYest($idSubDetailKomoditi){
        //         $query = $this->db->query("select avg(ab.satu) as rata1, avg(ab.dua) as rata2 from 
        //         (select idKecamatan,namaKecamatan,ifnull(avg(a.harga),0) as satu,ifnull(avg(b.harga),0) as dua from kecamatan left join pasar on idKecamatan=kecamatanIdKecamatan left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('2019-02-28', INTERVAL 0 DAY) and subDetailIdSubDetail = ".$idSubDetailKomoditi.") as a on pasar.idPasar=a.pasarIdPasar left join (select * from detailKomoditi where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('2019-02-28', INTERVAL 1 DAY) and subDetailIdSubDetail = ".$idSubDetailKomoditi.") as b on pasar.idPasar=b.pasarIdPasar group by idKecamatan) as ab where ab.satu != 0");
        //         return $query->row();
        // }

        public function getKomoditiSearch(){
                $query = $this->db->query("select concat('old-',idKomoditi) as idKomoditi,namaKomoditi,group_concat(idSubDetailKomoditi separator '|') as idSubDetail,group_concat(namaKomoditi,' ',namaSubDetailKomoditi separator '|') as namaSubKomoditi from komoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi group by idKomoditi");
                return $query->result_array();
        }

        public function getResultHarga($idKomoditi,$idKategoriKomoditi){
                $str = "";
                $stat = true;
                if(strstr($idKomoditi,"old-")){
                        $idKomoditi = str_replace('old-','',$idKomoditi);
                        $stat = false;
                }

                if($idKomoditi != 'ALL'){
                        if($stat){
                                $str = "where idSubDetailKomoditi=".$idKomoditi;
                        }else{
                                $str = "where idKomoditi =".$idKomoditi;
                        }
                        $str .= " and idKategoriKomoditi=".$idKategoriKomoditi;
                }else{
                        $str = "where idKategoriKomoditi=".$idKategoriKomoditi;
                }

                $query = $this->db->query("select concat('old-',idKomoditi) as idKomoditi,idKomoditi,namaKomoditi,group_concat(idSubDetailKomoditi order by namaKomoditi,namaSubDetailKomoditi separator '|') as idSubDetail,group_concat(namaKomoditi,' ',namaSubDetailKomoditi order by namaKomoditi,namaSubDetailKomoditi separator '|') as namaSubKomoditi,group_concat(namaSatuan order by namaKomoditi,namaSubDetailKomoditi separator '|') as satuan from komoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on satuanIdSatuan=idSatuan ".$str." group by idKomoditi order by namaKategoriKomoditi");
                return $query->result_array();
        }

        public function getResultSubKomoditiPrice($idSubDetail,$range,$idkec,$tanggal2,$idPasar){
                if($idkec != 'ALL'){
                        $str = "and kecamatanIdKecamatan=".$idkec;
                }else{
                        $str = "";
                }

                if($idPasar != 'ALL'){
                        $str .= " and pasarIdPasar=".$idPasar;
                }else{
                        $str .= "";
                }

                $query = $this->db->query("select avg(ifnull(a.harga,0)) as harga from subDetailKomoditi 
                left join (select harga,subDetailIdSubDetail from detailKomoditi left join pasar on idPasar=pasarIdPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal2."', INTERVAL ".$range." DAY) ".$str.") as a on idSubDetailKomoditi=a.subDetailIdSubDetail 
                where idSubDetailKomoditi = ".$idSubDetail." group by idSubDetailKomoditi");
                return $query->row();
        }

        public function getResultSubKomoditiPriceMonth($idSubDetail,$idKec,$ym,$idPasar){
                if($idKec != 'ALL'){
                        $str = "and kecamatanIdKecamatan=".$idKec;
                }else{
                        $str = "";
                }

                if($idPasar != 'ALL'){
                        $str .= " and pasarIdPasar=".$idPasar;
                }else{
                        $str .= "";
                }

                $query = $this->db->query("select avg(ifnull(a.harga,0)) as harga from subDetailKomoditi 
                left join (select harga,subDetailIdSubDetail from detailKomoditi left join pasar on idPasar=pasarIdPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m') = '".$ym."' ".$str.") as a on idSubDetailKomoditi=a.subDetailIdSubDetail 
                where idSubDetailKomoditi = ".$idSubDetail." group by idSubDetailKomoditi");
                return $query->row();
        }

        public function getResultParentKomoditiAvg($idKomoditi,$m,$idkec,$tanggal2){
                if($idkec != 'ALL'){
                        $str = "and pasar.kecamatanIdKecamatan = ".$idkec;
                }else{
                        $str = "";
                }
                $query = $this->db->query("select ceil(ifnull(avg(harga),0)) as harga from detailKomoditi left join subDetailKomoditi on subDetailIdSubDetail=idSubDetailKomoditi left join komoditi on idKomoditi=subDetailKomoditi.komoditiIdKomoditi left join pasar on idPasar=pasarIdPasar where idKomoditi = ".$idKomoditi." and DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal2."', INTERVAL ".$m." DAY)".$str);
                return $query->row();
        }

        function getLaporan1($tgglMulai,$tgglSelesai,$kecamatan,$pasar,$komoditi,$idKategoriKomoditi){
                // $str = "";
                // if($kecamatan != "ALL"){
                //         $str = " and kecamatanIdKecamatan = ".$kecamatan;
                // }else{
                //         $str = "";
                // }

                $str = "";
                $strq = "";
                $stat = true;
                if(strstr($komoditi,"old-")){
                        $komoditi = str_replace('old-','',$komoditi);
                        $stat = false;
                }

                if($komoditi != 'ALL'){
                        if($stat){
                                $str = "where idSubDetailKomoditi=".$komoditi;
                        }else{
                                $str = "where idKomoditi =".$komoditi;
                        }
                        $str .= " and kategoriIdKategoriKomoditi=".$idKategoriKomoditi;
                }else{
                        $str = "where kategoriIdKategoriKomoditi=".$idKategoriKomoditi;
                }

                if($kecamatan != 'ALL'){
                        $strq = "and kecamatanIdKecamatan=".$kecamatan;
                }else{
                        $strq = "";
                }

                if($pasar != 'ALL'){
                        $strq .= " and pasarIdPasar=".$pasar;
                }else{
                        $strq .= "";
                }

                $query = $this->db->query("select namaKomoditi,group_concat(namaKomoditi,' ',namaSubDetailKomoditi separator '|') as namaSubKomoditi,group_concat(ifnull(a.harga,0) separator '|') as harga1,group_concat(ifnull(b.harga,0) separator '|') as harga2,group_concat(namaSatuan separator '|') as namasatuan from komoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi left join satuan on idSatuan=satuanIdSatuan
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglMulai."' ".$strq." group by subDetailIdSubDetail) as a on idSubDetailKomoditi=a.subDetailIdSubDetail
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglSelesai."' ".$strq." group by subDetailIdSubDetail) as b on idSubDetailKomoditi=b.subDetailIdSubDetail ".$str." group by idKomoditi");
                return $query->result_array();
        }

        function getGrupNameMarket($kecamatan,$pasar){
                $str = "";
                if($kecamatan != "ALL"){
                        $str = " where kecamatanIdKecamatan = ".$kecamatan;
                }else{
                        $str = "";
                }

                $query = $this->db->query("select group_concat(namaPasar separator ', ') as namapasar from pasar left join kecamatan on idKecamatan=kecamatanIdKecamatan ".$str);
                return $query->row();
        }

        function getAllKecamatan(){
                $query = $this->db->query("select group_concat(namaKecamatan separator ', ') as namakecamatan from kecamatan");
                return $query->row();
        }

        function getNamaKecamatan($kecamatan){
                $query = $this->db->query("select * from kecamatan where idKecamatan=".$kecamatan);
                return $query->row();
        }

        function getAllPasar(){
                $query = $this->db->query("select group_concat(namaPasar separator ', ') as namaPasar from pasar");
                return $query->row();
        }

        function getAllPasarByKecamatan($kecamatan){
                $query = $this->db->query("select group_concat(namaPasar separator ', ') as namaPasar from pasar where kecamatanIdKecamatan=".$kecamatan);
                return $query->row();
        }

        function getNamaPasar($pasar){
                $query = $this->db->query("select * from pasar where idPasar=".$pasar);
                return $query->row();
        }

        function getLaporan2($tgglMulai,$tgglSelesai,$kecamatan){
                $str = "";
                if($kecamatan != "ALL"){
                        $str = " and kecamatanIdKecamatan = ".$kecamatan;
                }else{
                        $str = "";
                }

                $query = $this->db->query("select namaKategoriKomoditi,namaKomoditi,group_concat(namaKomoditi,' ',namaSubDetailKomoditi separator '|') as namaGroupKomoditi,group_concat(ifnull(a.harga,0) separator '|') as harga1,group_concat(ifnull(b.harga,0) separator '|') as harga2,group_concat(namaSatuan separator '|') as namasatuan from komoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi left join kategoriKomoditi on idKategoriKomoditi=kategoriIdKategoriKomoditi left join satuan on idSatuan=satuanIdSatuan
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglMulai."' ".$str." group by subDetailIdSubDetail) as a on idSubDetailKomoditi=a.subDetailIdSubDetail
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglSelesai."' ".$str." group by subDetailIdSubDetail) as b on idSubDetailKomoditi=b.subDetailIdSubDetail group by idKomoditi");
                return $query->result_array();
        }

        function getLaporan4($tgglMulaiFormat,$tgglSelesaiFormat,$kecamatan){
                $str = "";
                if($kecamatan != "ALL"){
                        $str = " and kecamatanIdKecamatan = ".$kecamatan;
                }else{
                        $str = "";
                }

                $query = $this->db->query("select namaKategoriKomoditi,namaKomoditi,group_concat(namaKomoditi,' ',namaSubDetailKomoditi separator '|') as namaGroupKomoditi,group_concat(ifnull(a.harga,0) separator '|') as harga1,group_concat(ifnull(b.harga,0) separator '|') as harga2,group_concat(namaSatuan separator '|') as namasatuan from komoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi left join kategoriKomoditi on idKategoriKomoditi=kategoriIdKategoriKomoditi left join satuan on idSatuan=satuanIdSatuan
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglMulaiFormat."' ".$str." group by subDetailIdSubDetail) as a on idSubDetailKomoditi=a.subDetailIdSubDetail
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglSelesaiFormat."' ".$str." group by subDetailIdSubDetail) as b on idSubDetailKomoditi=b.subDetailIdSubDetail where idKategoriKomoditi = 3 group by idKomoditi");
                return $query->result_array();
        }

	function getLaporan4Change($idKategoriKomoditi,$komoditi){
                $str = "";
                $stat = true;
                if(strstr($komoditi,"old-")){
                        $komoditi = str_replace('old-','',$komoditi);
                        $stat = false;
                }

                if($komoditi != 'ALL'){
                        if($stat){
                                $str = "where idSubDetailKomoditi=".$komoditi;
                        }else{
                                $str = "where idKomoditi =".$komoditi;
                        }
                        $str .= " and kategoriIdKategoriKomoditi=".$idKategoriKomoditi;
                }else{
                        $str = "where kategoriIdKategoriKomoditi=".$idKategoriKomoditi;
                }

                $query = $this->db->query("select concat('old-',idKomoditi) as idKomoditi,idKomoditi,namaKomoditi,group_concat(idSubDetailKomoditi separator '|') as idSubDetail,group_concat(namaKomoditi,' ',namaSubDetailKomoditi separator '|') as namaSubKomoditi,group_concat(namasatuan separator '|') as namasatuan from komoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi left join satuan on idSatuan=satuanIdSatuan ".$str." group by idKomoditi");
                return $query->result_array();
        }

        public function getHargaPerDate($idSubDetail,$range,$idkec,$tanggal2,$pasar){
                $str = "";
                if($idkec != 'ALL'){
                        $str = "and kecamatanIdKecamatan=".$idkec;
                }else{
                        $str = "";
                }

                if($pasar != 'ALL'){
                        $str .= " and pasarIdPasar=".$pasar;
                }else{
                        $str .= "";
                }

                $query = $this->db->query("select avg(ifnull(a.harga,0)) as harga from subDetailKomoditi 
                left join (select harga,subDetailIdSubDetail from detailKomoditi left join pasar on idPasar=pasarIdPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tanggal2."', INTERVAL ".$range." DAY) ".$str.") as a on idSubDetailKomoditi=a.subDetailIdSubDetail 
                where idSubDetailKomoditi = ".$idSubDetail." group by idSubDetailKomoditi");
                return $query->row();
        }

        public function getListKategoriKomoditi(){
                $query = $this->db->query("select idKategoriKomoditi,namaKategoriKomoditi from kategoriKomoditi order by namaKategoriKomoditi");
                return $query->result_array();
        }

        public function getListPasarByKecamatan($id){
                $query = $this->db->query("select * from pasar where kecamatanIdKecamatan=".$id);
                return $query->result_array();
        }

        public function getInfoKecamatan($idKec){
                $query = $this->db->query("select * from kecamatan where idKecamatan=".$idKec);
                return $query->row();
        }

        public function getKategoriKomoditi($idKomoditi){
                $str = "";
                $stat = true;
                if(strstr($idKomoditi,"old-")){
                        $idKomoditi = str_replace('old-','',$idKomoditi);
                        $stat = false;
                }

                if($idKomoditi != 'ALL'){
                        if($stat){
                                $str = "where idSubDetailKomoditi=".$idKomoditi;
                        }else{
                                $str = "where idKomoditi =".$idKomoditi;
                        }
                }else{
                        $str = "";
                }
                $query = $this->db->query("select idKategoriKomoditi,namaKategoriKomoditi from kategoriKomoditi left join komoditi on idKategoriKomoditi=kategoriIdKategoriKomoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi ".$str." group by idKategoriKomoditi order by orderByKategoriKomoditi");
                return $query->result_array();
        }

        public function getHargaPerKomoditi($idKomoditi,$tgglMulaiFormat,$tgglSelesaiFormat,$idKategoriKomoditi,$idKec,$idPasar){
                $str = "";
                $strq = "";
                $stat = true;
                if(strstr($idKomoditi,"old-")){
                        $idKomoditi = str_replace('old-','',$idKomoditi);
                        $stat = false;
                }

                if($idKomoditi != 'ALL'){
                        if($stat){
                                $str = "where idSubDetailKomoditi=".$idKomoditi;
                        }else{
                                $str = "where idKomoditi =".$idKomoditi;
                        }
                        $str .= " and kategoriIdKategoriKomoditi=".$idKategoriKomoditi;
                }else{
                        $str = "where kategoriIdKategoriKomoditi=".$idKategoriKomoditi;
                }

                if($idKec != 'ALL'){
                        $strq = "and kecamatanIdKecamatan=".$idKec;
                }else{
                        $strq = "";
                }

                if($idPasar != 'ALL'){
                        $strq .= " and pasarIdPasar=".$idPasar;
                }else{
                        $strq .= "";
                }

                $query = $this->db->query("select namaKomoditi,group_concat(namaKomoditi,' ',namaSubDetailKomoditi separator '|') as namaSubKomoditi,group_concat(ifnull(a.harga,0) separator '|') as harga1,group_concat(ifnull(b.harga,0) separator '|') as harga2,group_concat(namaSatuan separator '|') as namasatuan from komoditi left join subDetailKomoditi on idKomoditi=komoditiIdKomoditi left join satuan on idSatuan=satuanIdSatuan
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglMulaiFormat."' ".$strq." group by subDetailIdSubDetail) as a on idSubDetailKomoditi=a.subDetailIdSubDetail
                left join (select avg(harga) as harga,subDetailIdSubDetail from detailKomoditi left join pasar on pasarIdPasar=idPasar where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = '".$tgglSelesaiFormat."' ".$strq." group by subDetailIdSubDetail) as b on idSubDetailKomoditi=b.subDetailIdSubDetail ".$str." group by idKomoditi");
                return $query->result_array();
        }
}
?>