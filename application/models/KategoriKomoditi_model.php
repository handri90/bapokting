<?php
class KategoriKomoditi_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKategori(){
                $query = $this->db->query("select idKategoriKomoditi,namaKategoriKomoditi,orderByKategoriKomoditi from kategoriKomoditi order by orderByKategoriKomoditi");
                return $query->result_array();
        }

        public function inputKategoriKomoditi($kategorikomoditi,$urutan)
	{
                $query = $this->db->query("insert into kategoriKomoditi (namaKategoriKomoditi,orderByKategoriKomoditi) values ('".$kategorikomoditi."',".$urutan.")");
                return $query;
        }
        
        public function getKategori($id){
                $query = $this->db->query("select idKategoriKomoditi,namaKategoriKomoditi,orderByKategoriKomoditi from kategoriKomoditi where idKategoriKomoditi=".$id);
                return $query->row();
        }

        public function updateKategoriKomoditi($idkategori,$namakategori,$urutan){
                $query = $this->db->query("update kategoriKomoditi set namaKategoriKomoditi='".$namakategori."',orderByKategoriKomoditi=".$urutan." where idKategoriKomoditi=".$idkategori);
                return $query;
        }

        public function deleteKategori($id){
                $query = $this->db->query("select count(*) as jml from komoditi where kategoriIdKategoriKomoditi =".$id);
                $data = $query->row();
                if($data->jml > 0){
                        return false;
                }else{
                        $query = $this->db->query("delete from kategoriKomoditi where idKategoriKomoditi=".$id);
                        return $query;
                }
                
        }
}
?>