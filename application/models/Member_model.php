<?php
class Member_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListMember(){
          $query = $this->db->query("select * from member inner join hakAkses on `idHakAkses`=`hakAksesIdHakAkses`");
    		  return $query->result_array();
        }

        public function inputMember($namalengkap,$username,$passworduser,$leveluser,$filename){
          $query = $this->db->query("insert into member (namaMember,username,password,hakAksesIdHakAkses,filename) values ('".$namalengkap."','".$username."','".$passworduser."',".$leveluser.",'".$filename."')");
          return $query;
        }

        public function getMemberById($id){
          $query = $this->db->query("select * from member inner join hakAkses on `idHakAkses`=`hakAksesIdHakAkses` where idMember=".$id);
          return $query->row();
        }

        public function updateMember($namalengkap,$username,$leveluser,$iduser){
          $query = $this->db->query("update member set namaMember='".$namalengkap."',username='".$username."',hakAksesIdHakAkses=".$leveluser." where idMember=".$iduser);
          return $query;
        }

        public function deleteMember($id){
          $query = $this->db->query("delete from member where idMember=".$id);
          return $query;
        }

        public function getLevelMember(){
            $query = $this->db->query("select * from hakAkses");
    		  return $query->result_array();
        }

        public function getLevelMemberNoSuper(){
          $query = $this->db->query("select * from hakAkses where idHakAkses!=1");
    		  return $query->result_array();
        }

        public function getLevelMemberOnlyOne(){
          $query = $this->db->query("select * from hakAkses where idHakAkses=3");
    		  return $query->result_array();
        }

        public function changePasswordMember($passworduser,$iduser){
            $query = $this->db->query("update member set password='".$passworduser."' where idMember=".$iduser);
          return $query;
        }

        public function getListmemberNoSuper(){
          $query = $this->db->query("select * from member inner join hakAkses on `idHakAkses`=`hakAksesIdHakAkses` where hakAksesIdHakAkses!=1");
    		  return $query->result_array();
        }

        public function getListmemberOnlyOneUser($id){
          $query = $this->db->query("select * from member inner join hakAkses on `idHakAkses`=`hakAksesIdHakAkses` where idMember=".$id);
    		  return $query->result_array();
        }

        public function changefoto($filename,$iduser){
          $getfilename = $this->db->query("select filename from member where idMember=".$iduser);
          $qfilename = $getfilename->row();
          unlink('upload/fotouser/'.$qfilename->filename);
          $query = $this->db->query("update member set filename='".$filename."' where idMember=".$iduser);
          return $query;
        }
}
?>