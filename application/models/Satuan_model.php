<?php
class Satuan_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListSatuan(){
                $query = $this->db->query("select idSatuan,namaSatuan from satuan");
                return $query->result_array();
        }

        public function inputSatuan($satuan)
	{
                $query = $this->db->query("insert into satuan (namaSatuan) values ('".$satuan."')");
                return $query;
        }
        
        public function getSatuan($id){
                $query = $this->db->query("select idSatuan,namaSatuan from satuan where idSatuan=".$id);
                return $query->row();
        }

        public function updateSatuan($satuan,$idsatuan){
                $query = $this->db->query("update satuan set namaSatuan='".$satuan."' where idSatuan=".$idsatuan);
                return $query;
        }

        public function deleteSatuan($id){
                $query = $this->db->query("select count(*) as jml from subDetailKomoditi where satuanIdSatuan =".$id);
                $data = $query->row();
                if($data->jml > 0){
                        return false;
                }else{
                        $query = $this->db->query("delete from satuan where idSatuan=".$id);
                        return $query;
                }
                
        }
}
?>