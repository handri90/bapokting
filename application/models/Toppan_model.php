<?php
class Toppan_model extends CI_Model{
    public function getKomoditi($id = null){
        if($id == null){
            $query = $this->db->query("select * from kategoriKomoditi");
        }else{
            $query = $this->db->query("select * from kategoriKomoditi where idKategoriKomoditi=".$id);
        }
        return $query->result_array();
    }

    public function getKecamatan(){
        $query = $this->db->query("select namaKecamatan,idKecamatan from kecamatan");
        return $query->result_array();
    }

    public function getMarketPriceReport($tggl,$kecamatan,$idKomoditi){
        $str = '';
        $str2 = '';
        if($tggl != 'NOW'){
            $str = "'".$tggl."'";
        }else{
            $str = "curdate()";
        }

        if($kecamatan != 'ALL'){
            $str2 = "and idKecamatan =".$kecamatan;
        }else{
            $str2 = "";
        }

        $query = $this->db->query("select idSubDetailKomoditi,namaSatuan,concat(namaKomoditi,' ',namaSubDetailKomoditi) as namakomoditi,concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1,concat('Rp ', format(ifnull(avg(b.harga),0), 0)) as hargaavg2,if(ifnull(avg(a.harga),0) = ifnull(avg(b.harga),0) or (ifnull(avg(b.harga),0) = 0) or (ifnull(avg(a.harga),0) = 0),'SAMA',if(ifnull(avg(a.harga),0) > ifnull(avg(b.harga),0),'NAIK','TURUN')) as grafik,0 as viewType from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
        left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB(".$str.", INTERVAL 0 DAY) ".$str2.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail
        left join (select idDetailKomoditi,pasarIdPasar,subDetailIdSubDetail,tanggalHarga,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB(".$str.", INTERVAL 1 DAY) ".$str2.") as b on subDetailKomoditi.idSubDetailKomoditi=b.subDetailIdSubDetail where komoditiIdKomoditi=".$idKomoditi." group by idSubDetailKomoditi");
        return $query->result_array();
    }

    public function getDetailFrontMarketPrice($tggl,$idSubDetail,$idKecamatan){
        $str = '';

        if($idKecamatan != 'ALL'){
            $str = "and idKecamatan =".$idKecamatan;
        }else{
            $str = "";
        }

        $query = $this->db->query("select DATE_FORMAT(DATE_SUB('".$tggl."', INTERVAL 6 DAY),'%d-%m-%Y') as tanggal,
        concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1 from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
                left join (select tanggalHarga,subDetailIdSubDetail,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tggl."', INTERVAL 6 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail where idSubDetailKomoditi = ".$idSubDetail."
        union all
        select DATE_FORMAT(DATE_SUB('".$tggl."', INTERVAL 5 DAY),'%d-%m-%Y') as tanggal,
        concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1 from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
                left join (select subDetailIdSubDetail,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tggl."', INTERVAL 5 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail where idSubDetailKomoditi = ".$idSubDetail."
         union all
         select DATE_FORMAT(DATE_SUB('".$tggl."', INTERVAL 4 DAY),'%d-%m-%Y') as tanggal,
        concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1 from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
                left join (select subDetailIdSubDetail,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tggl."', INTERVAL 4 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail where idSubDetailKomoditi = ".$idSubDetail."
        union all
        select DATE_FORMAT(DATE_SUB('".$tggl."', INTERVAL 3 DAY),'%d-%m-%Y') as tanggal,
        concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1 from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
                left join (select subDetailIdSubDetail,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tggl."', INTERVAL 3 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail where idSubDetailKomoditi = ".$idSubDetail."
        union all
        select DATE_FORMAT(DATE_SUB('".$tggl."', INTERVAL 2 DAY),'%d-%m-%Y') as tanggal,
        concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1 from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
                left join (select subDetailIdSubDetail,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tggl."', INTERVAL 2 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail where idSubDetailKomoditi = ".$idSubDetail."
        union all
        select DATE_FORMAT(DATE_SUB('".$tggl."', INTERVAL 1 DAY),'%d-%m-%Y') as tanggal,
        concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1 from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
                left join (select subDetailIdSubDetail,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tggl."', INTERVAL 1 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail where idSubDetailKomoditi = ".$idSubDetail."
        union all
        select DATE_FORMAT(DATE_SUB('".$tggl."', INTERVAL 0 DAY),'%d-%m-%Y') as tanggal,
        concat('Rp ', format(ifnull(avg(a.harga),0), 0)) as hargaavg1 from subDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan
                left join (select subDetailIdSubDetail,harga from detailKomoditi left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') = DATE_SUB('".$tggl."', INTERVAL 0 DAY) ".$str.") as a on subDetailKomoditi.idSubDetailKomoditi=a.subDetailIdSubDetail where idSubDetailKomoditi = ".$idSubDetail);
        return $query->result_array();
    }

    public function getListHargaKomoditi($pasar,$komoditi,$tanggal){
        $str = "";
        $str2 = "";
        if($pasar != 'ALL'){
            $str = "and idPasar =".$pasar;
        }else{
            $str = "";
        }

        if($komoditi != 'ALL'){
            $str2 = "and idSubDetailKomoditi =".$komoditi;
        }else{
            $str2 = "";
        }
        $query = $this->db->query("select idDetailKomoditi,idPasar,idSubDetailKomoditi,concat(namaKecamatan,' ',namaPasar) as namaLokasi,concat(namaKomoditi,' - ',namaSubDetailKomoditi,' (',namaSatuan,')') as labelkomoditi,harga,DATE_FORMAT(`tanggalHarga`, '%d-%m-%Y') as tanggalHarga,idKategoriKomoditi from detailKomoditi left join subDetailKomoditi on subDetailIdSubDetail=idSubDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where DATE_FORMAT(`tanggalHarga`, '%d-%m-%Y') = '".$tanggal."' ".$str." ".$str2." order by idDetailKomoditi desc");
        return $query->result_array();
    }

    public function getListKecamatanPasar(){
        $query = $this->db->query("select idPasar,concat(namaKecamatan,' - ',namaPasar) as namaLokasi from pasar left join kecamatan on kecamatanIdKecamatan=idKecamatan");
        return $query->result_array();
    }

    public function getListKomoditi($pasar,$kategoriKomoditiSel,$tanggalAwal,$idSubDetailKomoditi){
        $str ="";
        if($idSubDetailKomoditi != "ALL"){
            $str .= "and subDetailIdSubDetail !=".$idSubDetailKomoditi;
        }
        $query = $this->db->query("select concat(`namaKomoditi`,' ',`namaSubDetailKomoditi`) as namasubdetailkomoditi,concat(`idSubDetailKomoditi`) as idsubdetailkomoditi,concat(`namaSatuan`) as satuan from `subDetailKomoditi` left join `komoditi` on `komoditiIdKomoditi`=`idKomoditi` left join `kategoriKomoditi` on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on satuanIdSatuan=idSatuan where idsubdetailkomoditi NOT IN (select subDetailIdSubDetail from detailKomoditi where DATE_FORMAT(tanggalHarga, '%Y-%m-%d') = '".$tanggalAwal."' and pasarIdPasar = '".$pasar."' ".$str.") and idKategoriKomoditi=".$kategoriKomoditiSel);
        return $query->result_array();
    }

    public function getListKomoditiSearch(){
        $query = $this->db->query("select concat(`namaKomoditi`,' ',`namaSubDetailKomoditi`) as namasubdetailkomoditi,concat(`idSubDetailKomoditi`) as idsubdetailkomoditi,concat(`namaSatuan`) as satuan from `subDetailKomoditi` left join `komoditi` on `komoditiIdKomoditi`=`idKomoditi` left join `kategoriKomoditi` on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on satuanIdSatuan=idSatuan");
        return $query->result_array();
    }

    public function inputHargaKomoditi($idPasar,$idKomoditi,$tanggal,$harga,$idMember){
        $query = $this->db->query("insert into detailKomoditi (pasarIdPasar,tanggalInputData,tanggalHarga,subDetailIdSubDetail,harga,memberIdMember) values (".$idPasar.",now(),concat(STR_TO_DATE('".$tanggal."', '%d-%m-%Y'),' ',curtime()),".$idKomoditi.",'".$harga."',".$idMember.")");
        return $this->db->affected_rows();
    }

    public function updateHargaKomoditi($idPasar,$idKomoditi,$tanggal,$harga,$idDetailKomoditi){
        $query = $this->db->query("update detailKomoditi set subDetailIdSubDetail=".$idKomoditi.",pasarIdPasar=".$idPasar.",tanggalHarga=concat(STR_TO_DATE('".$tanggal."', '%d-%m-%Y'),' ',curtime()),harga='".$harga."' where idDetailKomoditi=".$idDetailKomoditi);
        return $this->db->affected_rows();
    }

    public function deleteHargaKomoditi($idDetailKomoditi){
        $query = $this->db->query("delete from detailKomoditi where idDetailKomoditi=".$idDetailKomoditi);
        return $this->db->affected_rows();
    }

    public function get_user($username)
    {
            $query = $this->db->query("select idMember,namaMember,namaHakAkses,idHakAkses from member inner join hakAkses on `hakAksesIdHakAkses`=`idHakAkses` where username='".$username."'");
            return $query->result_array();
    }

	public function getFrontMobileKomoditi($id){
        $query = $this->db->query("select * from komoditi where kategoriIdKategoriKomoditi=".$id);
            return $query->result_array();
    }

    public function getKategoriKomoditi(){
        $query = $this->db->query("select idKategoriKomoditi,namaKategoriKomoditi from kategoriKomoditi");
            return $query->result_array();
    }

    public function cekValidData($idPasarField,$idKomoditiField,$tanggalField,$idDetailHargaKomoditi){
        $query = $this->db->query("select * from detailKomoditi where pasarIdPasar=".$idPasarField." and DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalField."' and subDetailIdSubDetail=".$idKomoditiField." and idDetailKomoditi !=".$idDetailHargaKomoditi);
                return $query->result_array();
    }
}
?>