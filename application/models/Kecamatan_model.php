<?php
class Kecamatan_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKecamatan(){
                $query = $this->db->query("select idKecamatan,namaKecamatan from kecamatan");
                return $query->result_array();
        }

        // public function inputKecamatan($pasar,$idKecamatan)
	// {
        //         $query = $this->db->query("insert into pasar (kecamatanIdKecamatan,namaPasar) values (".$idKecamatan.",'".$pasar."')");
        //         return $query;
        // }
        
        public function inputKecamatan($Kecamatan)
	{
                $query = $this->db->query("insert into kecamatan (namaKecamatan) values ('".$Kecamatan."')");
                return $query;
        }
        
        public function getKecamatan($id){
                $query = $this->db->query("select idKecamatan,namaKecamatan from kecamatan where idKecamatan=".$id);
                return $query->row();
        }

        public function updateKecamatan($Kecamatan,$idKecamatan){
                $query = $this->db->query("update kecamatan set namaKecamatan='".$Kecamatan."' where idKecamatan=".$idKecamatan);
                return $query;
        }

        public function deleteKecamatan($id){
                $query = $this->db->query("delete from kecamatan where idKecamatan=".$id);
                return $query;
        }
}
?>