<?php
class Pasar_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListPasar(){
                $query = $this->db->query("select idPasar,namaPasar,namaKecamatan from pasar left join kecamatan on kecamatanIdKecamatan=idKecamatan");
                return $query->result_array();
        }

        public function inputPasar($pasar,$idKecamatan)
	{
                $query = $this->db->query("insert into pasar (kecamatanIdKecamatan,namaPasar) values (".$idKecamatan.",'".$pasar."')");
                return $query;
        }
        
        public function getPasar($id){
                $query = $this->db->query("select idPasar,namaPasar,kecamatanIdKecamatan from pasar left join kecamatan on kecamatanIdKecamatan=idKecamatan where idPasar=".$id);
                return $query->row();
        }

        public function updatePasar($kecamatan,$pasar,$idpasar){
                $query = $this->db->query("update pasar set namaPasar='".$pasar."',kecamatanIdKecamatan=".$kecamatan." where idPasar=".$idpasar);
                return $query;
        }

        public function deletePasar($id){
                $query = $this->db->query("select count(*) as jml from detailKomoditi where pasarIdPasar =".$id);
                $data = $query->row();
                if($data->jml > 0){
                        return false;
                }else{
                        $query = $this->db->query("delete from pasar where idPasar=".$id);
                        return $query;
                }
                
        }
}
?>