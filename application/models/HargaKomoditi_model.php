<?php
class HargaKomoditi_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListHargaKomoditi($pasar,$komoditi,$tanggalharga){
                $str1 = "";
                if($pasar != 'ALL' & $komoditi != 'ALL'){
                        $str1 .= " WHERE idPasar IN (".$pasar.") AND idKomoditi IN (".$komoditi.") AND DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }else if ($pasar == 'ALL' & $komoditi != 'ALL') {
                        $str1 .= " WHERE idKomoditi IN (".$komoditi.") AND DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }else if ($pasar != 'ALL' & $komoditi == 'ALL') {
                        $str1 .= " WHERE idPasar IN (".$pasar.") AND DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }else{
                        $str1 .= " WHERE DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }

                $query = $this->db->query("select idDetailKomoditi,namaKecamatan,namaPasar,concat(namaKomoditi,' - ',namaSubDetailKomoditi) as labelkomoditi,namaSatuan,harga,DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') as tanggalHarga from detailKomoditi left join subDetailKomoditi on subDetailIdSubDetail=idSubDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan".$str1);
                return $query->result_array();
        }

        public function getListHargaKomoditiByUser($pasar,$komoditi,$tanggalharga,$user_id){
                $str1 = "";
                if($pasar != 'ALL' & $komoditi != 'ALL'){
                        $str1 .= " WHERE idPasar IN (".$pasar.") AND idKomoditi IN (".$komoditi.") AND DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }else if ($pasar == 'ALL' & $komoditi != 'ALL') {
                        $str1 .= " WHERE idKomoditi IN (".$komoditi.") AND DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }else if ($pasar != 'ALL' & $komoditi == 'ALL') {
                        $str1 .= " WHERE idPasar IN (".$pasar.") AND DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }else{
                        $str1 .= " WHERE DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggalharga."'";
                }

                $query = $this->db->query("select idDetailKomoditi,namaKecamatan,namaPasar,concat(namaKomoditi,' - ',namaSubDetailKomoditi) as labelkomoditi,namaSatuan,harga,DATE_FORMAT(`tanggalHarga`, '%Y-%m-%d') as tanggalHarga from detailKomoditi left join subDetailKomoditi on subDetailIdSubDetail=idSubDetailKomoditi left join komoditi on subDetailKomoditi.komoditiIdKomoditi=idKomoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on subDetailKomoditi.satuanIdSatuan=idSatuan left join pasar on pasarIdPasar=idPasar left join kecamatan on kecamatanIdKecamatan=idKecamatan".$str1." and memberIdMember=".$user_id);
                return $query->result_array();
        }

        public function inputHargaKomoditi($pasar,$tanggalharga,$komoditi,$harga,$memberId)
	{
                $query = $this->db->query("insert into detailKomoditi (pasarIdPasar,tanggalInputData,tanggalHarga,subDetailIdSubDetail,harga,memberIdMember) values (".$pasar.",now(),concat(STR_TO_DATE('".$tanggalharga."', '%d-%m-%Y'),' ',curtime()),".$komoditi.",'".$harga."',".$memberId.")");
                return $query;
        }
        
        public function getDetailHargaById($id){
                $query = $this->db->query("select idDetailKomoditi,pasarIdPasar,DATE_FORMAT(`tanggalHarga`, '%d-%m-%Y') as tanggalHarga,subDetailIdSubDetail,harga from detailKomoditi where idDetailKomoditi = ".$id);
                return $query->row();
        }

        public function updateHargaKomoditi($pasar,$tanggalharga,$komoditi,$harga,$idDetail){
                $query = $this->db->query("update detailKomoditi set pasarIdPasar=".$pasar.",tanggalHarga=concat(STR_TO_DATE('".$tanggalharga."', '%d-%m-%Y'),' ',curtime()),subDetailIdSubDetail=".$komoditi.",harga='".$harga."' where idDetailKomoditi=".$idDetail);
                return $query;
        }

        public function deleteDetailHarga($id){
                $query = $this->db->query("delete from detailKomoditi where idDetailKomoditi=".$id);
                return $query;
        }

        public function cekValidData($market,$tanggal,$detailid,$komoditiId){
                $query = $this->db->query("select * from detailKomoditi where pasarIdPasar=".$market." and DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggal."' and subDetailIdSubDetail=".$komoditiId." and idDetailKomoditi !=".$detailid);
                return $query->result_array();
        }
}
?>