<?php
class Komoditi_model extends CI_Model {

        public function __construct()
        {

        }

        public function getListKomoditas($kategori){
                $str ="";
                if($kategori != 'ALL'){
                        $str .= "where idKategoriKomoditi =".$kategori;
                }else{
                        $str .= "";
                }
                $query = $this->db->query("select idKomoditi,namaKomoditi,namaKategoriKomoditi from komoditi left join kategoriKomoditi on idKategoriKomoditi=kategoriIdKategoriKomoditi ".$str." order by orderByKategoriKomoditi,namaKomoditi");
                return $query->result_array();
        }

        public function getListKomoditi(){
                $query = $this->db->query("select idKomoditi,namaKomoditi,namaKategoriKomoditi from komoditi left join kategoriKomoditi on idKategoriKomoditi=kategoriIdKategoriKomoditi order by orderByKategoriKomoditi,namaKomoditi");
                return $query->result_array();
        }

        public function inputKomoditi($idKategori,$namakomoditi)
	{
                $query = $this->db->query("insert into komoditi (kategoriIdKategoriKomoditi,namaKomoditi) values (".$idKategori.",'".$namakomoditi."')");
                return $query;
	}

        public function getListKategoriKomoditiFormHarga(){
                $query = $this->db->query("select group_concat(`namaKomoditi`,' ',`namaSubDetailKomoditi` separator '|') as namasubdetailkomoditi,group_concat(`idSubDetailKomoditi` separator '|') as idsubdetailkomoditi,group_concat(`namaSatuan` separator '|') as satuan,concat(namaKategoriKomoditi,' ',namaKomoditi) as labeloptgroup from `subDetailKomoditi` left join `komoditi` on `komoditiIdKomoditi`=`idKomoditi` left join `kategoriKomoditi` on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on satuanIdSatuan=idSatuan group by idKomoditi");
                return $query->result_array();
        }

        public function getKomoditi($id){
                $query = $this->db->query("select idKomoditi,namaKomoditi,kategoriIdKategoriKomoditi from komoditi left join kategoriKomoditi on kategoriIdKategoriKomoditi=idKategoriKomoditi where idKomoditi=".$id);
                return $query->row();
        }

        public function updateKomoditi($idKategoriKomoditi,$namakomoditi,$idkomoditi){
                $query = $this->db->query("update komoditi set namaKomoditi='".$namakomoditi."',kategoriIdKategoriKomoditi=".$idKategoriKomoditi." where idKomoditi=".$idkomoditi);
                return $query;
        }

        public function deleteKomodoti($id){
                $query = $this->db->query("select count(*) as jml from subDetailKomoditi where komoditiIdKomoditi =".$id);
                $data = $query->row();
                if($data->jml > 0){
                        return false;
                }else{
                        $query = $this->db->query("delete from komoditi where idKomoditi=".$id);
                        return $query;
                }
                
        }

        public function getListKategoriKomoditiFormHargaNew($market,$tanggal,$detailid,$komoditiidsaved){
                $str = "";
                if(!empty($detailid)){
                        $str .= "and subDetailIdSubDetail !=".$komoditiidsaved;
                }
                $query = $this->db->query("select group_concat(`namaKomoditi`,' ',`namaSubDetailKomoditi` separator '|') as namasubdetailkomoditi,group_concat(`idSubDetailKomoditi` separator '|') as idsubdetailkomoditi,group_concat(`namaSatuan` separator '|') as satuan,concat(namaKategoriKomoditi,' ',namaKomoditi) as labeloptgroup from `subDetailKomoditi` left join `komoditi` on `komoditiIdKomoditi`=`idKomoditi` left join `kategoriKomoditi` on kategoriIdKategoriKomoditi=idKategoriKomoditi left join satuan on satuanIdSatuan=idSatuan where idsubdetailkomoditi NOT IN (select subDetailIdSubDetail from detailKomoditi where DATE_FORMAT(tanggalHarga, '%d-%m-%Y') = '".$tanggal."' and pasarIdPasar = ".$market." ".$str.") group by idKomoditi");
                return $query->result_array();
        }
}
?>