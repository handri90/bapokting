<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontier extends CI_Controller {

	public function index()
	{
		$this->frontierUser();
	}

	public function frontierUser(){
		 $data['listkecamatan'] = $this->Frontier_model->getListKecamatan();
		 $data['listkategorikomoditi'] = $this->Frontier_model->getListKategoriKomoditi();
		// $id = 'ALL';
		// $data['list'] = $this->Frontier_model->getListLaporan2($id);
		$data['content'] = 'content_front';
	    $this->load->view('panel_frontier',$data);
	}

	public function GetDataHargaPasar(){
		$id = $_POST['id'];
		$kat = $_POST['kat'];
		$datesearch = $_POST['datesearch'];
		$datesearch = explode('-',$datesearch);
		$datesearch = $datesearch[2]."-".$datesearch[1]."-".$datesearch[0];
		$reportfrontier = $this->Frontier_model->getListLaporan2($id,$kat,$datesearch);
		for($i=0;$i<count($reportfrontier);$i++){
			$persentase = 0;

			//persentase
			$priceToday = $reportfrontier[$i]['hargabiasa1'];
			$priceYesterday = $reportfrontier[$i]['hargabiasa2'];
			$stat = '';
			$stat2 = '';
			if($priceYesterday == '0'){
				$stat = '<i class="fa fa-pause"></i>';
				$stat2 = 'pricesame';
			}else if($priceToday == '0'){
				$stat = '<i class="fa fa-pause"></i>';
				$stat2 = 'pricesame';
			}else{
				if($priceToday > $priceYesterday){
					$stat = '<i class="fa fa-arrow-up"></i>';
					$stat2 = 'priceup';
				}else if($priceToday < $priceYesterday){
					$stat = '<i class="fa fa-arrow-down"></i>';
					$stat2 = 'pricedown';
				}else{
					$stat = '<i class="fa fa-pause"></i>';
					$stat2 = 'pricesame';
				}
			}

			if($priceYesterday != 0){
				$persentase = abs(round((($priceToday - $priceYesterday)/$priceYesterday)*100,2));
			}

			$rangePrice = $priceToday-$priceYesterday;
			//
			$reportfrontier[$i]['persentase'] = $persentase;
			$reportfrontier[$i]['arrowpersentase'] = $stat;
			$reportfrontier[$i]['classarrow'] = $stat2;
			$reportfrontier[$i]['rangePrice'] = abs($rangePrice);
		}
		echo json_encode($reportfrontier);
	}

	function getDetailHargaKomoditi(){
		$komId = $_POST['komId'];
    	$tanggal = $_POST['tanggal'];
		$idKec = $_POST['idKec'];
    	$tgglExpl = explode('-',$tanggal);
    	$tanggal = $tgglExpl[2]."-".$tgglExpl[1]."-".$tgglExpl[0];
		$reportDetailKomoditi = $this->Frontier_model->getKecamatanRataRata($komId,$idKec,$tanggal);

		//head
		$arr = array($this->setDateIndo(0,$tanggal),$this->setDateIndo(1,$tanggal),$this->setDateIndo(2,$tanggal),$this->setDateIndo(3,$tanggal),$this->setDateIndo(4,$tanggal));
		//end head
		
		//body
		$arrBody = array();
		$arrTempBody = array();
		$arrAll = array();
		$str = '';
		$strPerKec1 = 0;
		$strPerKec2 = 0;
		$strPerKec3 = 0;
		$strPerKec4 = 0;
		$strPerKec5 = 0;
		for($k=0;$k<count($reportDetailKomoditi);$k++){
			$reportDetKomoditi = $this->Frontier_model->getPricePasar($komId,$reportDetailKomoditi[$k]['idKecamatan'],$tanggal);
			$sep = "";
			if(!empty(count($reportDetKomoditi))){
				$sep = "<span class='fa fa-minus-circle' style='color:red'></span>&nbsp;&nbsp;";
			}
			
			$str = $sep."<strong class='kecamatantext'>".$reportDetailKomoditi[$k]['namaKecamatan']."</strong>".'|'.$this->formatDesimal($reportDetailKomoditi[$k]['lima']).'|'.$this->formatDesimal($reportDetailKomoditi[$k]['empat']).'|'.$this->formatDesimal($reportDetailKomoditi[$k]['tiga']).'|'.$this->formatDesimal($reportDetailKomoditi[$k]['dua']).'|'.$this->formatDesimal($reportDetailKomoditi[$k]['satu']).'|kecamatan'.'|'.$k;

			$strPerKec1 += $reportDetailKomoditi[$k]['lima'] == '-'?0:$reportDetailKomoditi[$k]['lima'];
			$strPerKec2 += $reportDetailKomoditi[$k]['empat'] == '-'?0:$reportDetailKomoditi[$k]['empat'];
			$strPerKec3 += $reportDetailKomoditi[$k]['tiga'] == '-'?0:$reportDetailKomoditi[$k]['tiga'];
			$strPerKec4 += $reportDetailKomoditi[$k]['dua'] == '-'?0:$reportDetailKomoditi[$k]['dua'];
			$strPerKec5 += $reportDetailKomoditi[$k]['satu'] == '-'?0:$reportDetailKomoditi[$k]['satu'];

			$arrTempBody = explode("|",$str);
			array_push($arrBody,$arrTempBody);

			//detailPasar
			$arrTempDetBody = array();
			$strDetBody = "";
			for($n=0;$n<count($reportDetKomoditi);$n++){
				$strDetBody = '<div style="padding-left:1.4em;">'.$reportDetKomoditi[$n]['namaPasar'].'</div>|'.$this->cekDashCenter($reportDetKomoditi[$n]['lima']).'|'.$this->cekDashCenter($reportDetKomoditi[$n]['empat']).'|'.$this->cekDashCenter($reportDetKomoditi[$n]['tiga']).'|'.$this->cekDashCenter($reportDetKomoditi[$n]['dua']).'|'.$this->cekDashCenter($reportDetKomoditi[$n]['satu']).'|pasar pasar-'.$k.'|'.$k;
				$arrTempDetBody = explode("|",$strDetBody);
				array_push($arrBody,$arrTempDetBody);
			}
			//end detail Pasar
		}
		//end body
		$strAll = array(array("satu"=>"Semua Kecamatan"),array("satu"=>nominal($strPerKec1)),array("satu"=>nominal($strPerKec2)),array("satu"=>nominal($strPerKec3)),array("satu"=>nominal($strPerKec4)),array("satu"=>nominal($strPerKec5)));

		$arrLast = array("datetittle"=>array_reverse($arr),"price"=>$arrBody,"avgall"=>$strAll);
		echo json_encode($arrLast);
	}

	function formatDesimal($k){
    	if($k == '-'){
                return "<div class='textcenter'><strong>".$k."</strong></div>";
            }else{
                return nominal($k);
            }
    }

	function setDateIndo($n,$tanggal){
		return array("satu"=>date("d M",strtotime( $tanggal."-".$n." days")),"dua"=>date("d M Y",strtotime( $tanggal.'-'.$n.' days' )),"tiga"=>date("d-m-Y",strtotime( $tanggal.'-'.$n.' days' )),"empat"=>date("d/m/Y",strtotime( $tanggal.'-'.$n.' days' )),"lima"=>date_and_month(date("Y-m-d",strtotime( $tanggal.'-'.$n.' days' ))));
	}

	function cekDashCenter($k){
		if($k == '-'){
			return "<div class='textcenter'><strong>".$k."</strong></div>";
		}else{
			return $k;
		}
	}

	public function tabelHarga(){
		if(!empty($_POST)){
			$idKomoditi = empty($_POST['komoditi'])?'ALL':$_POST['komoditi'];
			$idKec = empty($_POST['kecamatan'])?'ALL':$_POST['kecamatan'];
			$idPasar = empty($_POST['pasar'])?'ALL':$_POST['pasar'];
			$tanggalHargaMulai = $_POST['tanggalhargamulai'];
			$tanggalHargaSelesai = $_POST['tanggalhargaselesai'];
			$tipelaporan = $_POST['tipeLaporan'];
		}else{
			$idKomoditi = "ALL";
			$idKec = "ALL";
			$idPasar = "ALL";
			$tanggalHargaMulai = date("d-m-Y", mktime(0, 0, 0, date("m") , date("d")-6,date("Y")));
			$tanggalHargaSelesai = date("d-m-Y");
			$tipelaporan = 'tipelaporan1'; //default
		}

		//information
		if($idKec == 'ALL'){
			$data['labelinfokecamatan'] = 'Semua Kecamatan';
		}else{
			$infoKec = $this->Frontier_model->getInfoKecamatan($idKec);
			$data['labelinfokecamatan'] = $infoKec->namaKecamatan;
		}

		$table = "";
		if($tipelaporan == 'tipelaporan1'){
			$data['labeltipelaporan'] = 'Laporan Harian';
			$table = $this->generateDayReport($idKomoditi,$idKec,$idPasar,$tanggalHargaMulai,$tanggalHargaSelesai);
		}else if($tipelaporan == 'tipelaporan2'){
			$data['labeltipelaporan'] = 'Laporan Day to Day';
			$table = $this->generateDaytoDayReport($idKomoditi,$idKec,$idPasar,$tanggalHargaMulai,$tanggalHargaSelesai);
		}else{
			$data['labeltipelaporan'] = 'Laporan Bulanan';
			$table = $this->generateMonthReport($idKomoditi,$idKec,$idPasar,$tanggalHargaMulai,$tanggalHargaSelesai);
		}

		$data['listKomoditiSearch'] = $this->Frontier_model->getKomoditiSearch();
		$data['listkecamatan'] = $this->Frontier_model->getListKecamatan();
		if($idKec != 'ALL'){
			$data['listpasar'] = $this->Frontier_model->getListPasarByKecamatan($idKec);
		}

		$data['generatetablehead'] = $table['tablehead'];
		$data['generatetablebody'] = $table['tablebody'];
		$data['cacheform'] = array("idKec"=>$idKec,"idKomoditi"=>$idKomoditi,"idPasar"=>$idPasar,"tanggalmulai"=>$tanggalHargaMulai,"tanggalselesai"=>$tanggalHargaSelesai,"tipelaporan"=>$tipelaporan);
		$data['content'] = 'tabel_harga';
	    $this->load->view('panel_frontier',$data);
	}

	function generateDayReport($idKomoditi,$idKec,$idPasar,$tanggalHargaMulai,$tanggalHargaSelesai){
		$explRangeMulai = explode('-',$tanggalHargaMulai);
		$explRangeSelesai = explode('-',$tanggalHargaSelesai);
		$rangeMulai = date_create(date("d-m-Y", mktime(0, 0, 0, date($explRangeMulai[1]) , date($explRangeMulai[0]),date($explRangeMulai[2]))));
		$rangeSelesai = date_create(date("d-m-Y", mktime(0, 0, 0, date($explRangeSelesai[1]) , date($explRangeSelesai[0]),date($explRangeSelesai[2]))));
		$dateTemp = date_diff($rangeMulai,$rangeSelesai);
		
		$tanggal1 = explode('-',$tanggalHargaSelesai);
		$tanggal2 = date_format(date_create($tanggalHargaSelesai),"Y-m-d");
		$dataKategoriKomoditi = $this->Frontier_model->getKategoriKomoditi($idKomoditi);

		$rangeDate = $dateTemp->format("%a")+1;
		$tableBody = "";
		$tableBodyParent = "";
		$tableHead = "<tr><th class='head-no'>No</th><th scope='col'>Komoditas</th>";


		for($n=($rangeDate-1);$n>=0;--$n){
			$tableHead .= "<th scope='col'>".date("d-m-Y", mktime(0, 0, 0, date($tanggal1[1]) , date($tanggal1[0])-$n,date($tanggal1[2])))."</th>";
		}
		$tableHead .= "</tr>";

		// datakomoditas
		$no1 = 'A';
		for($z=0;$z<count($dataKategoriKomoditi);$z++){
			$tableBody .= "<tr><td class='bld-fnt'>".$no1++."</td><td scope='row' class='title-table'>".$dataKategoriKomoditi[$z]['namaKategoriKomoditi']."</td>";
			for($m=($rangeDate-1);$m>=0;--$m){
				// $dataParentKomoditi = $this->Frontier_model->getResultParentKomoditiAvg($dataList[$i]['idKomoditi'],$m,$idKec,$tanggal2);
				// $tableBody .= "<td class='texttengah'>".ceil($dataParentKomoditi->harga)."</td>";
				$tableBody .= "<td class='texttengah'>&nbsp;</td>";
			}
			$tableBody .= "</tr>";
			$dataList = $this->Frontier_model->getResultHarga($idKomoditi,$dataKategoriKomoditi[$z]['idKategoriKomoditi']);
			$no2 = 1;
			for($i=0;$i<count($dataList);$i++){
				$idSubDetail = explode('|',$dataList[$i]['idSubDetail']);
				$namaSubKomoditi = explode('|',$dataList[$i]['namaSubKomoditi']);
            	$satuan = explode('|',$dataList[$i]['satuan']);
				$tableBody .= "<tr><td class='bld-fnt'>".number_to_roman($no2++)."</td><td scope='row' class='sub-title-table'>".$dataList[$i]['namaKomoditi']."</td>";
				//rata2 per komoditi
				for($t=($rangeDate-1);$t>=0;--$t){
					// $dataParentKomoditi = $this->Frontier_model->getResultParentKomoditiAvg($dataList[$i]['idKomoditi'],$m,$idKec,$tanggal2);
					// $tableBody .= "<td class='texttengah'>".ceil($dataParentKomoditi->harga)."</td>";
					$tableBody .= "<td class='texttengah'>&nbsp;</td>";
				}
				$tableBody .= "</tr>";

				//banyak komoditas 
				$no3 = 1;
				for($k=0;$k<count($idSubDetail);$k++){
					$tableBody .= "<tr><td class='fnt-cntr'>".$no3++."</td><td scope='row' class='sub-data-title-table'>".$namaSubKomoditi[$k]." / ".$satuan[$k]."</td>";

					//harga berapa hari
					for($g=($rangeDate-1);$g>=0;--$g){
						$dataSub = $this->Frontier_model->getResultSubKomoditiPrice($idSubDetail[$k],$g,$idKec,$tanggal2,$idPasar);
						$tableBody .= "<td class='texttengah'>".nominal(ceil($dataSub->harga))."</td>";
					}
					$tableBody .= "</tr>";
				}
			}
		}

		return array('tablehead'=>$tableHead,'tablebody'=>$tableBody);
	}

	public function generateDaytoDayReport($idKomoditi,$idKec,$idPasar,$tanggalHargaMulai,$tanggalHargaSelesai){
		$tgglMulaiFormat = date_format(date_create($tanggalHargaMulai),"Y-m-d");
		$tgglSelesaiFormat = date_format(date_create($tanggalHargaSelesai),"Y-m-d");
		$dataKategoriKomoditi = $this->Frontier_model->getKategoriKomoditi($idKomoditi);
		
		$tableBody = "";
		$tableBodyParent = "";
		$tableHead = "<tr><th class='head-no'>No</th><th scope='col'>Komoditas</th>";
		$tableHead .= "<th scope='col'>".$tanggalHargaMulai."</th><th scope='col'>".$tanggalHargaSelesai."</th>";
		$tableHead .= "</tr>";

		// datakomoditas
		$no1 = 'A';
		for($i=0;$i<count($dataKategoriKomoditi);$i++){
			
			$tableBody .= "<tr><td class='bld-fnt'>".$no1++."</td><td scope='row' class='title-table'>".$dataKategoriKomoditi[$i]['namaKategoriKomoditi']."</td><td>&nbsp;</td><td>&nbsp;</td></tr>";

			$dataHargaPerKomoditi = $this->Frontier_model->getHargaPerKomoditi($idKomoditi,$tgglMulaiFormat,$tgglSelesaiFormat,$dataKategoriKomoditi[$i]['idKategoriKomoditi'],$idKec,$idPasar);
			$no2 = 1;
			for($k=0;$k<count($dataHargaPerKomoditi);$k++){
				$tableBody .= "<tr><td class='bld-fnt'>".number_to_roman($no2++)."</td><td scope='row' class='sub-title-table'>".$dataHargaPerKomoditi[$k]['namaKomoditi']."</td><td>&nbsp;</td><td>&nbsp;</td></tr>";
				$namaSubKomoditi = explode('|',$dataHargaPerKomoditi[$k]['namaSubKomoditi']);
				$harga1 = explode('|',$dataHargaPerKomoditi[$k]['harga1']);
				$harga2 = explode('|',$dataHargaPerKomoditi[$k]['harga2']);
				$namasatuan = explode('|',$dataHargaPerKomoditi[$k]['namasatuan']);
				$no3 = 1;
				for($n=0;$n<count($namaSubKomoditi);$n++){
					$tableBody .= "<tr><td class='fnt-cntr'>".$no3++."</td><td scope='row' class='sub-data-title-table'>".$namaSubKomoditi[$n]." / ".$namasatuan[$n]."</td><td class='texttengah'>".nominal(ceil($harga1[$n]))."</td><td class='texttengah'>".nominal(ceil($harga2[$n]))."</td></tr>";
				}
			}
		}

		return array('tablehead'=>$tableHead,'tablebody'=>$tableBody);
	}

	public function generateMonthReport($idKomoditi,$idKec,$idPasar,$tanggalHargaMulai,$tanggalHargaSelesai){
		$tgglMulaiFormat = date_format(date_create($tanggalHargaMulai),"Y-m-d");
		$tgglSelesaiFormat = date_format(date_create($tanggalHargaSelesai),"Y-m-d");
		$h = date("Ym", strtotime($tgglMulaiFormat));
		
		

		$tableBody = "";
		$tableHead = "<tr><th class='head-no'>No</th><th scope='col'>Komoditas</th>";

		$dataKategoriKomoditi = $this->Frontier_model->getKategoriKomoditi($idKomoditi);
		
		while($h <= date("Ym", strtotime($tgglSelesaiFormat))){
			$year = substr($h, 0, 4);
			$month = substr($h, 4, 2);
			$tableHead .= "<th scope='col'>".month_year_new($year."-".$month)."</th>";
			if(substr($h, 4, 2) == "12")
				$h = (date("Y", strtotime($h."01")) + 1)."01";
			else
				$h++;
		}
		$tableHead .= "</tr>";
		
		$no1 = 'A';
		for($z=0;$z<count($dataKategoriKomoditi);$z++){
			$tableBody .= "<tr><td class='bld-fnt'>".$no1++."</td><td scope='row' class='title-table'>".$dataKategoriKomoditi[$z]['namaKategoriKomoditi']."</td>";
			$o = date("Ym", strtotime($tgglMulaiFormat));
			while($o <= date("Ym", strtotime($tgglSelesaiFormat))){
				$tableBody .= "<td class='texttengah'>&nbsp;</td>";
				if(substr($o, 4, 2) == "12")
					$o = (date("Y", strtotime($o."01")) + 1)."01";
				else
					$o++;
			}
			$tableBody .= "</tr>";
			$dataList = $this->Frontier_model->getResultHarga($idKomoditi,$dataKategoriKomoditi[$z]['idKategoriKomoditi']);
			$no2 = 1;
			for($i=0;$i<count($dataList);$i++){
				$idSubDetail = explode('|',$dataList[$i]['idSubDetail']);
				$namaSubKomoditi = explode('|',$dataList[$i]['namaSubKomoditi']);
            	$satuan = explode('|',$dataList[$i]['satuan']);
				$tableBody .= "<tr><td class='bld-fnt'>".number_to_roman($no2++)."</td><td scope='row' class='sub-title-table'>".$dataList[$i]['namaKomoditi']."</td>";
				$a = date("Ym", strtotime($tgglMulaiFormat));
				while($a <= date("Ym", strtotime($tgglSelesaiFormat))){
					$tableBody .= "<td class='texttengah'>&nbsp;</td>";
					if(substr($a, 4, 2) == "12")
						$a = (date("Y", strtotime($a."01")) + 1)."01";
					else
						$a++;
				}
				$tableBody .= "</tr>";

				//banyak komoditas 
				$no3 = 1;
				for($k=0;$k<count($idSubDetail);$k++){
					$tableBody .= "<tr><td class='fnt-cntr'>".$no3++."</td><td scope='row' class='sub-data-title-table'>".$namaSubKomoditi[$k]." / ".$satuan[$k]."</td>";
					$g = date("Ym", strtotime($tgglMulaiFormat));
					while($g <= date("Ym", strtotime($tgglSelesaiFormat))){
						$year = substr($g, 0, 4);
						$month = substr($g, 4, 2);
						$ym = $year."-".$month;
						$dataSub = $this->Frontier_model->getResultSubKomoditiPriceMonth($idSubDetail[$k],$idKec,$ym,$idPasar);
						$tableBody .= "<td class='texttengah'>".nominal(ceil($dataSub->harga))."</td>";
						if(substr($g, 4, 2) == "12")
							$g = (date("Y", strtotime($g."01")) + 1)."01";
						else
							$g++;
					}
					$tableBody .= "</tr>";
				}
			}
		}
		return array('tablehead'=>$tableHead,'tablebody'=>$tableBody);
	}

	public function generateDownload(){
		//query
		$komoditi = $_GET['komoditi'];
		$kecamatan = $_GET['kecamatan'];
		$pasar = $_GET['pasar'];
		$tgglMulai = $_GET['tgglMulai'];
		$tgglSelesai = $_GET['tgglSelesai'];
		$formatlaporan = $_GET['formatlaporan'];

		if($formatlaporan == 'tipelaporan2'){
			$this->formatLaporan1($komoditi,$kecamatan,$tgglMulai,$tgglSelesai,$pasar);
		}else if($formatlaporan == 'tipelaporan1'){
			$this->formatLaporan4($komoditi,$kecamatan,$tgglMulai,$tgglSelesai,$pasar);
		}else if($formatlaporan == 'tipelaporan3'){
			$this->formatLaporan3($komoditi,$kecamatan,$tgglMulai,$tgglSelesai,$pasar);
		}
		// else if($formatlaporan == 'laporan3'){
		// 	$this->formatLaporan3($komoditi,$kecamatan,$tgglMulai,$tgglSelesai);
		// }else{
		// 	$this->formatLaporan4($komoditi,$kecamatan,$tgglMulai,$tgglSelesai);
		// }
	}

	public function setoptions(){
		$config['cacheable']    = true;
        $config['cachedir']     = './assets/qrcode/';
        $config['errorlog']     = './assets/qrcode/';
        $config['imagedir']     = './assets/qrcode/images/';
        $config['quality']      = true;
        $config['size']         = '1024';
        $config['black']        = array(224,255,255);
		$config['white']        = array(70,130,180);
		return $config;
	}

	public function makeQRCODE($objPHPExcel,$tgglSelesaiFormat,$coord){
		$this->ciqrcode->initialize($this->setoptions());
		$image_name='qrcode.png';

		$params['data'] = 'Disperindagkop UKM Kobar'.date_indo($tgglSelesaiFormat);
        $params['level'] = 'H';
        $params['size'] = 10;
        $params['savename'] = FCPATH.'./assets/qrcode/images/'.$image_name;
		$this->ciqrcode->generate($params);

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Disperindagkop UKM Kabupaten Kotawaringin Bara');
		$objDrawing->setDescription('Disperindagkop UKM Kabupaten Kotawaringin Bara');
		$objDrawing->setPath('./assets/qrcode/images/qrcode.png');
		$objDrawing->setCoordinates($coord); 
		$objDrawing->setHeight(150);
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
	}

	public function makeBarcode($objPHPExcel,$tgglSelesaiFormat,$coord){
		$this->zend->load('Zend/Barcode');
		$imageResource = Zend_Barcode::factory('code128', 'image', array('text'=>'Disperindagkop UKM Kobar'.date_indo($tgglSelesaiFormat)), array())->draw();
		$imageName = 'barcode.jpg';
		$imagePath = './assets/qrcode/images/'; // penyimpanan file barcode
		imagejpeg($imageResource, $imagePath.$imageName); 
		$pathBarcode = $imagePath.$imageName;

		$objDrawing = new PHPExcel_Worksheet_Drawing();
		$objDrawing->setName('Disperindagkop UKM');
		$objDrawing->setDescription('Disperindagkop UKM');
		$objDrawing->setPath('./assets/qrcode/images/barcode.jpg');
		$objDrawing->setCoordinates($coord); 
		$objDrawing->setWorksheet($objPHPExcel->getActiveSheet());
	}

	function formatLaporan1($komoditi,$kecamatan,$tgglMulai,$tgglSelesai,$pasar){
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

		$komoditi = $komoditi;
		$kecamatan = $kecamatan;
		$tgglMulaiFormat = date_format(date_create($tgglMulai),"Y-m-d");
		$tgglSelesaiFormat = date_format(date_create($tgglSelesai),"Y-m-d");

		if($kecamatan != 'ALL'){
			$listkecamatan = $this->Frontier_model->getNamaKecamatan($kecamatan);
			$namakecamatan = $listkecamatan->namaKecamatan;
		}else{
			$listkecamatan = $this->Frontier_model->getAllKecamatan();
			$namakecamatan = $listkecamatan->namakecamatan;
		}

		if($pasar != 'ALL'){
			$listpasar = $this->Frontier_model->getNamaPasar($pasar);
			$namaPasar = $listpasar->namaPasar;
		}else{
			if($kecamatan != 'ALL'){
				$listpasar = $this->Frontier_model->getAllPasarByKecamatan($kecamatan);
				$namaPasar = $listpasar->namaPasar;
			}else{
				$listpasar = $this->Frontier_model->getAllPasar();
				$namaPasar = $listpasar->namaPasar;
			}
		}


		$objPHPExcel = new PHPExcel();

		//text center
		$style1 = array(
			'font' => array(
				'size' => 11,
				'bold' => true
			),'alignment'=> array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			)
		);

		$style2 = array(
			'borders' => array(
				'allborders' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'font' => array(
				'bold' => true
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
		);

		$style3 = array(
			'borders' => array(
				'allborders' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'font' => array(
				'bold' => true
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				),'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'E1E0F7')
				)
		);

		$style31 = array(
			'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'a2b5cb')
				)
		);

		$style32 = array(
			'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'd8d8d8')
				)
		);

		$style4 = array(
			'borders' => array(
				'allborders' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'font' => array(
				'bold' => true
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
				)
		);

		$stylekolomkategorikomoditi = array(
			'borders' => array(
				'allborders' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			),
			'font' => array(
				'bold' => true
				)
		);

		$stylebold = array(
			'borders' => array(
				'allborders' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'font' => array(
				'bold' => true,
				'size' => 11
				)
		);

		$styledata = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				)
				);

		$stylechilparent = array(
			'alignment'=> array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			)
		);
    
    	$stylecenter = array(
        	'alignment'=> array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			)
        );

		$objPHPExcel->getProperties()->setCreator("");
		$objPHPExcel->getProperties()->setLastModifiedBy("");
		$objPHPExcel->getProperties()->setTitle("");
		$objPHPExcel->getProperties()->setSubject("");
		$objPHPExcel->getProperties()->setDescription("");

		$objPHPExcel->setActiveSheetIndex(0);

		$filename = "Laporan Day to Day (".date_indo($tgglSelesaiFormat).").xlsx";
		$objPHPExcel->getActiveSheet()->setTitle("Task-Overview");

		// $objPHPExcel->getActiveSheet()->getStyle("A2:D4")->applyFromArray($style1);
		$objPHPExcel->getActiveSheet()->getStyle("A8:G8")->applyFromArray($style2);
		// $objPHPExcel->getActiveSheet()->getStyle("A9:H9")->applyFromArray($style3);
		$objPHPExcel->getActiveSheet()->getStyle("A9:G9")->applyFromArray($style4);
		$objPHPExcel->getActiveSheet()->getStyle("A8:G9")->applyFromArray($style31);
    	$objPHPExcel->getActiveSheet()->getStyle("A8:C8")->applyFromArray($stylecenter);

		$objPHPExcel->getActiveSheet()->mergeCells("A2:C2");
		$objPHPExcel->getActiveSheet()->mergeCells("A8:A9");
		$objPHPExcel->getActiveSheet()->mergeCells("B8:B9");
		$objPHPExcel->getActiveSheet()->mergeCells("C8:C9");
		$objPHPExcel->getActiveSheet()->mergeCells("B5:K5");
		$objPHPExcel->getActiveSheet()->mergeCells("B6:K6");
		// $objPHPExcel->getActiveSheet()->mergeCells("A3:C3");
		// $objPHPExcel->getActiveSheet()->mergeCells("A4:H4");
		// $objPHPExcel->getActiveSheet()->mergeCells("F7:G7");
		// $objPHPExcel->getActiveSheet()->mergeCells("D3:H3");

		$objPHPExcel->getActiveSheet()->SetCellValue("A2","Informasi Perkembangan Harga");
		//$objPHPExcel->getActiveSheet()->SetCellValue("D2",":".$listPasar->namapasar);
		$objPHPExcel->getActiveSheet()->SetCellValue("A3","Provinsi");
		$objPHPExcel->getActiveSheet()->SetCellValue("B3",": Kalimantan Tengah");
		$objPHPExcel->getActiveSheet()->SetCellValue("A4","Kabupaten");
		$objPHPExcel->getActiveSheet()->SetCellValue("B4",": Kotawaringin Barat");
		$objPHPExcel->getActiveSheet()->SetCellValue("A5","Kecamatan");
		$objPHPExcel->getActiveSheet()->SetCellValue("B5",": ".$namakecamatan);
		$objPHPExcel->getActiveSheet()->SetCellValue("A6","Pasar");
		$objPHPExcel->getActiveSheet()->SetCellValue("B6",": ".$namaPasar);
		// $objPHPExcel->getActiveSheet()->SetCellValue("A4","Tanggal Pengamatan : ".date_indo($tgglSelesaiFormat));
		// $objPHPExcel->getActiveSheet()->SetCellValue("D3",":".date_indo($tgglSelesaiFormat));

		$objPHPExcel->getActiveSheet()->mergeCells("F8:G8");

		$objPHPExcel->getActiveSheet()->SetCellValue("A8","No");
		$objPHPExcel->getActiveSheet()->SetCellValue("B8","Komoditas");
		$objPHPExcel->getActiveSheet()->SetCellValue("C8","Satuan");
		// $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');
		// $objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);
		$objPHPExcel->getActiveSheet()->SetCellValue("D8","Harga Kemarin");
		$objPHPExcel->getActiveSheet()->SetCellValue("E8","Harga Sekarang");
		$objPHPExcel->getActiveSheet()->SetCellValue("F8","Perubahan");
		// die;
		// $objPHPExcel->getActiveSheet()->SetCellValue("H8","KETERANGAN");

		// $objPHPExcel->getActiveSheet()->SetCellValue("A9","1");
		// $objPHPExcel->getActiveSheet()->SetCellValue("B9","2");
		// $objPHPExcel->getActiveSheet()->SetCellValue("C9","3");
		// $objPHPExcel->getActiveSheet()->SetCellValue("D9","4");
		// $objPHPExcel->getActiveSheet()->SetCellValue("E9","5");
		// $objPHPExcel->getActiveSheet()->SetCellValue("F9","6");
		// $objPHPExcel->getActiveSheet()->SetCellValue("H9","7");

		$objPHPExcel->getActiveSheet()->SetCellValue("A9","");
		$objPHPExcel->getActiveSheet()->SetCellValue("B9","");
		$objPHPExcel->getActiveSheet()->SetCellValue("C9","");
		$objPHPExcel->getActiveSheet()->SetCellValue("D9",$tgglMulai);
		$objPHPExcel->getActiveSheet()->SetCellValue("E9",$tgglSelesai);
		$objPHPExcel->getActiveSheet()->SetCellValue("F9","Rp.");
		$objPHPExcel->getActiveSheet()->SetCellValue("G9","%");
		// $objPHPExcel->getActiveSheet()->SetCellValue("H9","");

		$numberColumn = 10;
		$letterCount = 'A';
		$dataKategoriKomoditi = $this->Frontier_model->getKategoriKomoditi($komoditi);
		for($k=0;$k<count($dataKategoriKomoditi);$k++){
			$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,$letterCount);
			$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($stylekolomkategorikomoditi);
			$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$dataKategoriKomoditi[$k]['namaKategoriKomoditi']);
			$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($stylebold);
			$objPHPExcel->getActiveSheet()->SetCellValue("C".$numberColumn,"");
			$objPHPExcel->getActiveSheet()->SetCellValue("D".$numberColumn,"");
			$objPHPExcel->getActiveSheet()->SetCellValue("E".$numberColumn,"");
			$objPHPExcel->getActiveSheet()->SetCellValue("F".$numberColumn,"");
			$objPHPExcel->getActiveSheet()->SetCellValue("G".$numberColumn,"");
			// $objPHPExcel->getActiveSheet()->SetCellValue("H".$numberColumn,"");
			$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn.":G".$numberColumn)->applyFromArray($style32);

			$dataList = $this->Frontier_model->getLaporan1($tgglMulaiFormat,$tgglSelesaiFormat,$kecamatan,$pasar,$komoditi,$dataKategoriKomoditi[$k]['idKategoriKomoditi']);

			$numberColumn++;
			$noroman = 1;
			for($i=0;$i<count($dataList);$i++){
				$no = 1;
				$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,number_to_roman($noroman++));
				$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($stylekolomkategorikomoditi);
				$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$dataList[$i]['namaKomoditi']);
				$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($stylebold);
				$objPHPExcel->getActiveSheet()->SetCellValue("C".$numberColumn,"");
				$objPHPExcel->getActiveSheet()->SetCellValue("D".$numberColumn,"");
				$objPHPExcel->getActiveSheet()->SetCellValue("E".$numberColumn,"");
				$objPHPExcel->getActiveSheet()->SetCellValue("F".$numberColumn,"");
				$objPHPExcel->getActiveSheet()->SetCellValue("G".$numberColumn,"");
				// $objPHPExcel->getActiveSheet()->SetCellValue("H".$numberColumn,"");
				$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn.":G".$numberColumn)->applyFromArray($styledata);

				$namasatuan = explode('|',$dataList[$i]['namasatuan']);
				$namaSubKomoditi = explode('|',$dataList[$i]['namaSubKomoditi']);
				$harga1 = explode('|',$dataList[$i]['harga1']);
				$harga2 = explode('|',$dataList[$i]['harga2']);
				$numberColumn++;
				for($n=0;$n<count($namaSubKomoditi);$n++){
					$persentase = 0;
					$result = 0;
					$strHarga = "";
					$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,$no);
					$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($stylechilparent);
					$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$namaSubKomoditi[$n]);
					$objPHPExcel->getActiveSheet()->SetCellValue("C".$numberColumn,$namasatuan[$n]);
					$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn)->applyFromArray($stylechilparent);
					$objPHPExcel->getActiveSheet()->SetCellValue("D".$numberColumn,"Rp      ".nominal($harga1[$n]));
					$objPHPExcel->getActiveSheet()->SetCellValue("E".$numberColumn,"Rp      ".nominal($harga2[$n]));
					$result = $harga2[$n]-$harga1[$n];
					
					$objPHPExcel->getActiveSheet()->SetCellValue("F".$numberColumn,'       '.nominal(abs($result)));
					if($harga1[$n] != 0){
						$persentase = abs(round((($harga2[$n] - $harga1[$n])/$harga1[$n])*100,2));
					}
				
					if($result < 0){
						$persentase = "(".$persentase.")";
					}
					
					$objPHPExcel->getActiveSheet()->SetCellValue("G".$numberColumn,'       '.$persentase);
					// $objPHPExcel->getActiveSheet()->SetCellValue("H".$numberColumn,"        ");
					$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn.":G".$numberColumn)->applyFromArray($styledata);

					$numberColumn++;
					$no++;
				}
			}
			$letterCount++;
		}

		$jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$col = 'A';
		while(true){
			$objPHPExcel->getActiveSheet()->getStyle($col."9:".$col.$jmlColumn)->applyFromArray($styledata);
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}

		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);

// 		$this->makeQRCODE($objPHPExcel,$tgglSelesaiFormat,"A".($jmlColumn+2));

// 		$this->makeBarcode($objPHPExcel,$tgglSelesaiFormat,"C".($jmlColumn+2));

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		// unlink("./assets/qrcode/images/qrcode.png");
		// unlink("./assets/qrcode/images/barcode.jpg");
		exit;
}

function formatLaporan3($komoditi,$kecamatan,$tgglMulai,$tgglSelesai,$pasar){
	require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
	require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

	$tgglMulaiFormat = date_format(date_create($tgglMulai),"Y-m-d");
	$tgglSelesaiFormat = date_format(date_create($tgglSelesai),"Y-m-d");
	$h = date("Ym", strtotime($tgglMulaiFormat));
	$i = date("Ym", strtotime($tgglMulaiFormat));

	$styledata = array(
		'borders' => array(
			'outline' => array(
			   'style' => PHPExcel_Style_Border::BORDER_THIN
			   )
			)
		);

		$style1 = array(
			'font' => array(
				'bold' => true
			),'alignment'=> array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),'borders' => array(
				'allborders' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'a2b5cb')
				)
		);

		$style3 = array(
			'font' => array(
				'bold' => true
			),'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

		$style3new = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

		$style2 = array(
			'font' => array(
				'bold' => true
			),'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				)
		);

		$style2new = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				)
		);

		$style5 = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				)
		);

		$style18 = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

		$style5new = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'font' => array(
					'bold' => true
				)
		);

		$style1new = array(
			'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'd8d8d8')
				)
		);

		$stylenew = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

	if($kecamatan != 'ALL'){
		$listkecamatan = $this->Frontier_model->getNamaKecamatan($kecamatan);
		$namakecamatan = $listkecamatan->namaKecamatan;
	}else{
		$listkecamatan = $this->Frontier_model->getAllKecamatan();
		$namakecamatan = $listkecamatan->namakecamatan;
	}

	if($pasar != 'ALL'){
		$listpasar = $this->Frontier_model->getNamaPasar($pasar);
		$namaPasar = $listpasar->namaPasar;
	}else{
		if($kecamatan != 'ALL'){
			$listpasar = $this->Frontier_model->getAllPasarByKecamatan($kecamatan);
			$namaPasar = $listpasar->namaPasar;
		}else{
			$listpasar = $this->Frontier_model->getAllPasar();
			$namaPasar = $listpasar->namaPasar;
		}
	}


	$objPHPExcel = new PHPExcel();

	$objPHPExcel->getProperties()->setCreator("");
	$objPHPExcel->getProperties()->setLastModifiedBy("");
	$objPHPExcel->getProperties()->setTitle("");
	$objPHPExcel->getProperties()->setSubject("");
	$objPHPExcel->getProperties()->setDescription("");

	$objPHPExcel->setActiveSheetIndex(0);

	$filename = "Laporan Bulanan.xlsx";
	$objPHPExcel->getActiveSheet()->setTitle("Task-Overview");

	$objPHPExcel->getActiveSheet()->mergeCells("A2:C2");
	$objPHPExcel->getActiveSheet()->mergeCells("A8:A9");
	$objPHPExcel->getActiveSheet()->mergeCells("B8:B9");
	$objPHPExcel->getActiveSheet()->mergeCells("C8:C9");
	$objPHPExcel->getActiveSheet()->mergeCells("B5:K5");
	$objPHPExcel->getActiveSheet()->mergeCells("B6:K6");

	$objPHPExcel->getActiveSheet()->SetCellValue("A2","Informasi Perkembangan Harga");
	//$objPHPExcel->getActiveSheet()->SetCellValue("D2",":".$listPasar->namapasar);
	$objPHPExcel->getActiveSheet()->SetCellValue("A3","Provinsi");
	$objPHPExcel->getActiveSheet()->SetCellValue("B3",": Kalimantan Tengah");
	$objPHPExcel->getActiveSheet()->SetCellValue("A4","Kabupaten");
	$objPHPExcel->getActiveSheet()->SetCellValue("B4",": Kotawaringin Barat");
	$objPHPExcel->getActiveSheet()->SetCellValue("A5","Kecamatan");
	$objPHPExcel->getActiveSheet()->SetCellValue("B5",": ".$namakecamatan);
	$objPHPExcel->getActiveSheet()->SetCellValue("A6","Pasar");
	$objPHPExcel->getActiveSheet()->SetCellValue("B6",": ".$namaPasar);

	$objPHPExcel->getActiveSheet()->SetCellValue("A8","No");
	$objPHPExcel->getActiveSheet()->SetCellValue("B8","Komoditas");
	$objPHPExcel->getActiveSheet()->SetCellValue("C8","Satuan");
	$objPHPExcel->getActiveSheet()->SetCellValue("D8","Harga (Rp.)");

	$colx = "C";
	while($h <= date("Ym", strtotime($tgglSelesaiFormat))){
		$colx++;
		if(substr($h, 4, 2) == "12")
			$h = (date("Y", strtotime($h."01")) + 1)."01";
		else
			$h++;
	}
	$objPHPExcel->getActiveSheet()->mergeCells("D8:".$colx."8");

	$cold = "C";
	while($i <= date("Ym", strtotime($tgglSelesaiFormat))){
		$cold++;
		$year = substr($i, 0, 4);
		$month = substr($i, 4, 2);
		$objPHPExcel->getActiveSheet()->SetCellValue($cold."9",month_year_new($year."-".$month));
		if(substr($i, 4, 2) == "12")
			$i = (date("Y", strtotime($i."01")) + 1)."01";
		else
			$i++;
	}

	
	$colx++;
	$objPHPExcel->getActiveSheet()->SetCellValue($colx."8","Harga Rata-Rata (Rp.)");
	$objPHPExcel->getActiveSheet()->mergeCells($colx."8:".$colx."9");

	$objPHPExcel->getActiveSheet()->getStyle("A8:".$colx."9")->applyFromArray($style1);





	$dataKategoriKomoditi = $this->Frontier_model->getKategoriKomoditi($komoditi);
		
		$no1 = 'A';
		$numberColumn = 10;
		for($z=0;$z<count($dataKategoriKomoditi);$z++){
			$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,$no1++);
			$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($style3);
			$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$dataKategoriKomoditi[$z]['namaKategoriKomoditi']);
			$objPHPExcel->getActiveSheet()->SetCellValue("C".$numberColumn,"");
			$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($style2);
			$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn)->applyFromArray($style5);

			$cold1 = "C";
			$k = date("Ym", strtotime($tgglMulaiFormat));
			while($k <= date("Ym", strtotime($tgglSelesaiFormat))){
				$cold1++;
				$objPHPExcel->getActiveSheet()->SetCellValue($cold1.$numberColumn,"");
				$objPHPExcel->getActiveSheet()->getStyle($cold1.$numberColumn)->applyFromArray($stylenew);
				if(substr($k, 4, 2) == "12")
					$k = (date("Y", strtotime($k."01")) + 1)."01";
				else
					$k++;
			}
			$cold1++;
			$objPHPExcel->getActiveSheet()->getStyle($cold1.$numberColumn)->applyFromArray($stylenew);
			$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn.":".$cold1.$numberColumn)->applyFromArray($style1new);
			$numberColumn++;
			$dataList = $this->Frontier_model->getResultHarga($komoditi,$dataKategoriKomoditi[$z]['idKategoriKomoditi']);
			$no2 = 1;
			for($i=0;$i<count($dataList);$i++){
				$idSubDetail = explode('|',$dataList[$i]['idSubDetail']);
				$namaSubKomoditi = explode('|',$dataList[$i]['namaSubKomoditi']);
				$satuan = explode('|',$dataList[$i]['satuan']);
				$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,number_to_roman($no2++));
				$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$dataList[$i]['namaKomoditi']);
				$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($style3);
				$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($style5new);
				$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn)->applyFromArray($style5);

				$cold2 = "C";
				$a = date("Ym", strtotime($tgglMulaiFormat));
				while($a <= date("Ym", strtotime($tgglSelesaiFormat))){
					$cold2++;
					$objPHPExcel->getActiveSheet()->SetCellValue($cold2.$numberColumn,"");
					$objPHPExcel->getActiveSheet()->getStyle($cold2.$numberColumn)->applyFromArray($stylenew);
					if(substr($a, 4, 2) == "12")
						$a = (date("Y", strtotime($a."01")) + 1)."01";
					else
						$a++;
				}
				$cold2++;
				$objPHPExcel->getActiveSheet()->getStyle($cold2.$numberColumn)->applyFromArray($stylenew);
				$numberColumn++;

				//banyak komoditas 
				$no3 = 1;
				for($r=0;$r<count($idSubDetail);$r++){
					$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,$no3++);
					$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$namaSubKomoditi[$r]);
					$objPHPExcel->getActiveSheet()->SetCellValue("C".$numberColumn,$satuan[$r]);
					$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($style3new);
					$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($style2new);
					$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn)->applyFromArray($style18);

					$cold3 = "C";
					$g = date("Ym", strtotime($tgglMulaiFormat));
					$sumPrice = 0;
					$jmlPembagi = 0;
					while($g <= date("Ym", strtotime($tgglSelesaiFormat))){
						$year = substr($g, 0, 4);
						$month = substr($g, 4, 2);
						$ym = $year."-".$month;
						$dataSub = $this->Frontier_model->getResultSubKomoditiPriceMonth($idSubDetail[$r],$kecamatan,$ym,$pasar);
						$sumPrice += $dataSub->harga;
						if($dataSub->harga != '0'){
							$jmlPembagi++;
						}
						
						$cold3++;
						$objPHPExcel->getActiveSheet()->SetCellValue($cold3.$numberColumn,nominal(ceil($dataSub->harga)));
						$objPHPExcel->getActiveSheet()->getStyle($cold3.$numberColumn)->applyFromArray($stylenew);
						if(substr($g, 4, 2) == "12")
							$g = (date("Y", strtotime($g."01")) + 1)."01";
						else
							$g++;
					}
					if($jmlPembagi != 0){
						$avg = $sumPrice/$jmlPembagi;
					}else{
						$avg = 0;
					}
					$cold3++;
					$objPHPExcel->getActiveSheet()->SetCellValue($cold3.$numberColumn,nominal(ceil($avg)));
					$objPHPExcel->getActiveSheet()->getStyle($cold3.$numberColumn)->applyFromArray($stylenew);	

					$numberColumn++;
				}
			}
		}













	$jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
	$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

	$objPHPExcel->getActiveSheet()->getStyle("A8:".$jmlRow.$jmlColumn)->applyFromArray($styledata);

	$col = 'A';
	while(true){
		//$objPHPExcel->getActiveSheet()->getStyle($col."10:".$col.$jmlColumn)->applyFromArray($styledata);
		$tempCol = $col++;
		$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
		if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
			break;
		}
	}

	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');
	$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);

// 	$this->makeQRCODE($objPHPExcel,$tgglSelesaiFormat,"A".($jmlColumn+2));

// 	$this->makeBarcode($objPHPExcel,$tgglSelesaiFormat,"C".($jmlColumn+2));

	header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
	header('Content-Disposition: attachment;filename="'.$filename.'"');
	header('Cache-Control: max-age=0');

	$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
	
	$writer->save('php://output');
	// unlink("./assets/qrcode/images/qrcode.png");
	// unlink("./assets/qrcode/images/barcode.jpg");
	exit;
}

	function formatLaporan4($komoditi,$kecamatan,$tgglMulai,$tgglSelesai,$pasar){
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
		require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');

		//query
		$komoditi = $komoditi;
		$kecamatan = $kecamatan;
		$tgglMulaiFormat = date_format(date_create($tgglMulai),"Y-m-d");
		$tgglSelesaiFormat = date_format(date_create($tgglSelesai),"Y-m-d");

		if($kecamatan != 'ALL'){
			$listkecamatan = $this->Frontier_model->getNamaKecamatan($kecamatan);
			$namakecamatan = $listkecamatan->namaKecamatan;
		}else{
			$listkecamatan = $this->Frontier_model->getAllKecamatan();
			$namakecamatan = $listkecamatan->namakecamatan;
		}

		if($pasar != 'ALL'){
			$listpasar = $this->Frontier_model->getNamaPasar($pasar);
			$namaPasar = $listpasar->namaPasar;
		}else{
			if($kecamatan != 'ALL'){
				$listpasar = $this->Frontier_model->getAllPasarByKecamatan($kecamatan);
				$namaPasar = $listpasar->namaPasar;
			}else{
				$listpasar = $this->Frontier_model->getAllPasar();
				$namaPasar = $listpasar->namaPasar;
			}
		}

		//start selisih hari
		$explRangeMulai = explode('-',$tgglMulai);
		$explRangeSelesai = explode('-',$tgglSelesai);
		$rangeMulai = date_create(date("d-m-Y", mktime(0, 0, 0, date($explRangeMulai[1]) , date($explRangeMulai[0]),date($explRangeMulai[2]))));
		$rangeSelesai = date_create(date("d-m-Y", mktime(0, 0, 0, date($explRangeSelesai[1]) , date($explRangeSelesai[0]),date($explRangeSelesai[2]))));
		$dateTemp = date_diff($rangeMulai,$rangeSelesai);
		$rangeDate = $dateTemp->format("%a")+1;
		//end selisih hari

		//head table
		$tanggal1 = explode('-',$tgglSelesai);
		//end head
		
		$tanggal2 = date_format(date_create($tgglSelesai),"Y-m-d");

		$objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("");
		$objPHPExcel->getProperties()->setLastModifiedBy("");
		$objPHPExcel->getProperties()->setTitle("");
		$objPHPExcel->getProperties()->setSubject("");
		$objPHPExcel->getProperties()->setDescription("");

		$objPHPExcel->setActiveSheetIndex(0);

		$filename = "Laporan Harian (".date_indo($tgglSelesaiFormat).").xlsx";
		
		$objPHPExcel->getActiveSheet()->setTitle("Task-Overview");

		$stylehead = array(
			'font' => array(
				'bold' => true,
				'size' => 13
			),'alignment'=> array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER
			)
		);

		$style1 = array(
			'font' => array(
				'bold' => true
			),'alignment'=> array(
				'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
				'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
			),'borders' => array(
				'allborders' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'a2b5cb')
				)
		);

		$style1new = array(
			'fill' => array(
					'type' => PHPExcel_Style_Fill::FILL_SOLID,
					'color' => array('rgb' => 'd8d8d8')
				)
		);

		$stylenew = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

		$styledata = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				)
			);

		$style3new = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

		$style3 = array(
			'font' => array(
				'bold' => true
			),'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

		$style2 = array(
			'font' => array(
				'bold' => true
			),'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				)
		);

		$style5 = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				)
		);

		$style5new = array(
			'borders' => array(
				'outline' => array(
				   'style' => PHPExcel_Style_Border::BORDER_THIN
				   )
				),'alignment'=> array(
					'horizontal'=> PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
					'vertical' => PHPExcel_Style_Alignment::VERTICAL_CENTER,
				)
		);

		$objPHPExcel->getActiveSheet()->mergeCells("A7:A8");
		$objPHPExcel->getActiveSheet()->mergeCells("B7:B8");
		$objPHPExcel->getActiveSheet()->mergeCells("C7:C8");
		$objPHPExcel->getActiveSheet()->mergeCells("B4:K4");
		$objPHPExcel->getActiveSheet()->mergeCells("B5:K5");
		
		$colx = "C";
		for($k=0;$k<$rangeDate;$k++){
			$colx++;
		}
		$objPHPExcel->getActiveSheet()->mergeCells("D7:".$colx."7");
		$colx++;
		$objPHPExcel->getActiveSheet()->mergeCells($colx."7:".$colx."8");
		$objPHPExcel->getActiveSheet()->mergeCells("A1:D1");
		// $objPHPExcel->getActiveSheet()->mergeCells("A2:D2");

		$objPHPExcel->getActiveSheet()->SetCellValue("A1","Informasi Perkembangan Harga");
		$objPHPExcel->getActiveSheet()->SetCellValue("A2","Provinsi");
		$objPHPExcel->getActiveSheet()->SetCellValue("B2",": Kalimantan Tengah");
		$objPHPExcel->getActiveSheet()->SetCellValue("A3","Kabupaten");
		$objPHPExcel->getActiveSheet()->SetCellValue("B3",": Kotawaringin Barat");
		$objPHPExcel->getActiveSheet()->SetCellValue("A4","Kecamatan");
		$objPHPExcel->getActiveSheet()->SetCellValue("B4",": ".$namakecamatan);
		$objPHPExcel->getActiveSheet()->SetCellValue("A5","Pasar");
		$objPHPExcel->getActiveSheet()->SetCellValue("B5",": ".$namaPasar);
		$objPHPExcel->getActiveSheet()->SetCellValue("A7","No");
		$objPHPExcel->getActiveSheet()->SetCellValue("B7","Komoditas");
		$objPHPExcel->getActiveSheet()->SetCellValue("C7","Satuan");
		$objPHPExcel->getActiveSheet()->SetCellValue("D7","Harga (Rp.)");
		$objPHPExcel->getActiveSheet()->SetCellValue($colx."7","Harga Rata-rata (Rp.)");				

		$colHead = "D";
		for($n=($rangeDate-1);$n>=0;--$n){
			$objPHPExcel->getActiveSheet()->SetCellValue($colHead."8",date("d-m-Y", mktime(0, 0, 0, date($tanggal1[1]) , date($tanggal1[0])-$n,date($tanggal1[2]))));
			$colHead++;
		}

		$objPHPExcel->getActiveSheet()->getStyle("A7:".$colx."8")->applyFromArray($style1);
		// $objPHPExcel->getActiveSheet()->getStyle("A1:A2")->applyFromArray($stylehead);


		$numberColumn = 9;
		$letterCount = 'A';
		$no1 = 'A';
		$tempColX = 0;
		$dataKategoriKomoditi = $this->Frontier_model->getKategoriKomoditi($komoditi);
		for($k=0;$k<count($dataKategoriKomoditi);$k++){
			$dataList = $this->Frontier_model->getLaporan4Change($dataKategoriKomoditi[$k]['idKategoriKomoditi'],$komoditi);
			$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,$no1);
			$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($style3);

			$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$dataKategoriKomoditi[$k]['namaKategoriKomoditi']);
			$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($style2);
			$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn)->applyFromArray($style5);
			$colY = "D";
			for($j=($rangeDate-1);$j>=0;--$j){
				$objPHPExcel->getActiveSheet()->getStyle($colY.$numberColumn)->applyFromArray($stylenew);
				$colY++;
			}
			$objPHPExcel->getActiveSheet()->getStyle($colY.$numberColumn)->applyFromArray($stylenew);
			$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn.":".$colY.$numberColumn)->applyFromArray($style1new);

			$no1++;
			$numberColumn++;
			$no2 = 1;
			for($i=0;$i<count($dataList);$i++){
				$idSubDetail = explode('|',$dataList[$i]['idSubDetail']);
				$namaSubKomoditi = explode('|',$dataList[$i]['namaSubKomoditi']);
				$namasatuan = explode('|',$dataList[$i]['namasatuan']);

				$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,number_to_roman($no2));
				$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($style3);

				$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$dataList[$i]['namaKomoditi']);
				$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($style2);
				$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn)->applyFromArray($style5);
				$tempColX = $numberColumn;
				//banyak komoditas 
				$no2++;
				$no3 = 1;
				for($n=0;$n<count($idSubDetail);$n++){
					$colY = "D";
					$numberColumn++;
					$objPHPExcel->getActiveSheet()->SetCellValue("A".$numberColumn,$no3);
					$objPHPExcel->getActiveSheet()->getStyle("A".$numberColumn)->applyFromArray($style3new);
				
					$objPHPExcel->getActiveSheet()->SetCellValue("B".$numberColumn,$namaSubKomoditi[$n]);
					$objPHPExcel->getActiveSheet()->getStyle("B".$numberColumn)->applyFromArray($style5);

					$objPHPExcel->getActiveSheet()->SetCellValue("C".$numberColumn,$namasatuan[$n]);
					$objPHPExcel->getActiveSheet()->getStyle("C".$numberColumn)->applyFromArray($style5new);


					//harga berapa hari
					$sumPrice = 0;
					$jmlPembagi = 0;
					for($z=($rangeDate-1);$z>=0;--$z){
						$dataSub = $this->Frontier_model->getHargaPerDate($idSubDetail[$n],$z,$kecamatan,$tanggal2,$pasar);
						$objPHPExcel->getActiveSheet()->SetCellValue($colY.$numberColumn,nominal(ceil($dataSub->harga)));
						$objPHPExcel->getActiveSheet()->getStyle($colY.$numberColumn)->applyFromArray($stylenew);
						$objPHPExcel->getActiveSheet()->getStyle($colY.$tempColX)->applyFromArray($stylenew);
						$sumPrice += $dataSub->harga;
						if($dataSub->harga != '0'){
							$jmlPembagi++;
						}
						$colY++;
					}
					if($jmlPembagi != 0){
						$avg = $sumPrice/$jmlPembagi;
					}else{
						$avg = 0;
					}
					
					$objPHPExcel->getActiveSheet()->SetCellValue($colY.$numberColumn,nominal(ceil($avg)));	
					$objPHPExcel->getActiveSheet()->getStyle($colY.$numberColumn)->applyFromArray($stylenew);				
					$no3++;
				}
				$objPHPExcel->getActiveSheet()->getStyle($colY.$numberColumn)->applyFromArray($stylenew);					
				$numberColumn++;
			}					
		}

		$jmlRow = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
		$jmlColumn = $objPHPExcel->getActiveSheet()->getHighestRow();

		$objPHPExcel->getActiveSheet()->getStyle("A7:".$jmlRow.$jmlColumn)->applyFromArray($styledata);

		$col = 'A';
		while(true){
			//$objPHPExcel->getActiveSheet()->getStyle($col."8:".$col.$jmlColumn)->applyFromArray($styledata);
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setWidth('10');
		$objPHPExcel->getActiveSheet()->getColumnDimension('C')->setAutoSize(false);

// 		$this->makeQRCODE($objPHPExcel,$tgglSelesaiFormat,"A".($jmlColumn+2));

// 		$this->makeBarcode($objPHPExcel,$tgglSelesaiFormat,"C".($jmlColumn+2));

		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		// unlink("./assets/qrcode/images/qrcode.png");
		// unlink("./assets/qrcode/images/barcode.jpg");
		exit;
	}

	public function GetListPasar(){
		$id = $this->input->post('id');
		$pasar = $this->Frontier_model->getListPasarByKecamatan($id);
		$html = "";
		for($i = 0;$i<count($pasar);$i++){
			$html .= "<option value='".$pasar[$i]['idPasar']."'>".$pasar[$i]['namaPasar']."</option>";
		}
		echo $html;
	}
}