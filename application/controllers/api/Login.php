<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Login extends REST_Controller{

    function __construct($config = 'rest') {
        parent::__construct($config);
    }

    public function index_get(){

        $username = $this->get('username');
        $password = $this->get('password');
        
        $user = $this->Login_model->cek_user_login($username);
        if(isset($user->password)){
            
            if(password_verify($password, $user->password)){
                $user = $this->Toppan_model->get_user($username);
                $this->response([
                    'message' => 'true',
                    'dataku' => $user
                ], REST_Controller::HTTP_OK);
            }else{
                $this->response([
                    'message' => 'false',
                    'data' => 'Tidak ditemukan member'
                ], REST_Controller::HTTP_OK);
            }
        }else{
            $this->response([
                'message' => 'false',
                'data' => 'Coba lagi'
            ], REST_Controller::HTTP_OK);
        }
    }

}

?>