<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Toppan extends REST_Controller{

    function __construct($config = 'rest') {
        parent::__construct($config);
    }

    public function marketprice_get(){
        $tggl = $this->get("tanggal");
        $kecamatan = $this->get("kecamatan");
        $tempArray = array();
        $kategorikomoditi = $this->Toppan_model->getKategoriKomoditi();
        for($i=0;$i<count($kategorikomoditi);$i++){
            array_push($tempArray,array("idSubDetailKomoditi"=>"","namaSatuan"=>"","namakomoditi"=>$kategorikomoditi[$i]['namaKategoriKomoditi'],"hargaavg1"=>"","hargaavg2"=>"","grafik"=>"","viewType"=>2));
            $komoditi = $this->Toppan_model->getFrontMobileKomoditi($kategorikomoditi[$i]['idKategoriKomoditi']);
            for($k=0;$k<count($komoditi);$k++){
                array_push($tempArray,array("idSubDetailKomoditi"=>"","namaSatuan"=>"","namakomoditi"=>$komoditi[$k]['namaKomoditi'],"hargaavg1"=>"","hargaavg2"=>"","grafik"=>"","viewType"=>1));
                $price = $this->Toppan_model->getMarketPriceReport($tggl,$kecamatan,$komoditi[$k]['idKomoditi']);
                for($j=0;$j<count($price);$j++){
                    array_push($tempArray,$price[$j]);
                }
            }
        }

        if($tempArray){
            $this->response([
                'status' => true,
                'data' => $tempArray
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function detailfrontmarketprice_get(){
        $tggl = $this->get("tanggal");
        $idSubDetail = $this->get("idSubDetail");
        $idKecamatan = $this->get("idKecamatan");
        $price = $this->Toppan_model->getDetailFrontMarketPrice($tggl,$idSubDetail,$idKecamatan);

        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function kecamatan_get(){
        $price = $this->Toppan_model->getKecamatan();

        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function hargakomoditi_get(){
        $tggl = $this->get("tanggal");
        $komoditi = $this->get("komoditi");
        $pasar = $this->get("pasar");
        $expTanggal = explode("-",$tggl);
        if(strlen($expTanggal[1]) == 1){
            $expTanggal[1] = "0".$expTanggal[1];
        }
    
    	if(strlen($expTanggal[0]) == 1){
            $expTanggal[0] = "0".$expTanggal[0];
        }
    
        $tanggal = $expTanggal[0]."-".$expTanggal[1]."-".$expTanggal[2];
        $price = $this->Toppan_model->getListHargaKomoditi($pasar,$komoditi,$tanggal);

        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'data' => 'No Data'
            ], REST_Controller::HTTP_OK);
        }
    }

    public function hargakomoditi_post(){
        $idPasar = $this->post("idPasar");
        $idKomoditi = $this->post("idKomoditi");
        $tanggal = $this->post("tanggal");
        $harga = $this->post("harga");
        $idMember = $this->post("idMember");

        $price = $this->Toppan_model->inputHargaKomoditi($idPasar,$idKomoditi,$tanggal,$harga,$idMember);

        if($price > 0){
            $this->response([
                'status' => true,
                'message' => 'sukses'
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function hargakomoditi_put(){
        $idPasar = $this->put("idPasar");
        $idKomoditi = $this->put("idKomoditi");
        $tanggal = $this->put("tanggal");
        $harga = $this->put("harga");
        $idDetailKomoditi = $this->put("idDetailKomoditi");

        $price = $this->Toppan_model->updateHargaKomoditi($idPasar,$idKomoditi,$tanggal,$harga,$idDetailKomoditi);

        if($price > 0){
            $this->response([
                'status' => true,
                'message' => 'sukses'
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function hargakomoditi_delete(){
        $idDetailKomoditi = $this->delete("idDetailKomoditi");

        $price = $this->Toppan_model->deleteHargaKomoditi($idDetailKomoditi);

        if($price > 0){
            $this->response([
                'status' => true,
                'message' => 'sukses'
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

	public function hargakomoditidel_post(){
    	$idDetailKomoditi = $this->post("idDetailKomoditi");

        $price = $this->Toppan_model->deleteHargaKomoditi($idDetailKomoditi);

        if($price > 0){
            $this->response([
                'status' => true,
                'message' => 'sukses'
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function kecamatanpasar_get(){
        $price = $this->Toppan_model->getListKecamatanPasar();

        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function komoditi_get(){
        $pasar = $this->get("pasar");
        $kategoriKomoditiSel = $this->get("kategoriKomoditiSel");
        $tanggalAwal = $this->get("tanggalAwal");
        $idSubDetailKomoditi = $this->get("idSubDetailKomoditi");
        $price = $this->Toppan_model->getListKomoditi($pasar,$kategoriKomoditiSel,$tanggalAwal,$idSubDetailKomoditi);

        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function komoditisearch_get(){
        $price = $this->Toppan_model->getListKomoditiSearch();

        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function kategorikomoditi_get(){
        $price = $this->Toppan_model->getKategoriKomoditi();

        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

    public function cekvaliddata_get(){
        $idPasarField = $this->get("idPasarField");
        $idKomoditiField = $this->get("idKomoditiField");
        $tanggalField = $this->get("tanggalField");
        $idDetailHargaKomoditi = $this->get("idDetailHargaKomoditi");

        $expTanggal = explode("-",$tanggalField);
        if(strlen($expTanggal[1]) == 1){
            $expTanggal[1] = "0".$expTanggal[1];
        }
    
    	if(strlen($expTanggal[0]) == 1){
            $expTanggal[0] = "0".$expTanggal[0];
        }
    
        $tanggalField = $expTanggal[0]."-".$expTanggal[1]."-".$expTanggal[2];
        $price = $this->Toppan_model->cekValidData($idPasarField,$idKomoditiField,$tanggalField,$idDetailHargaKomoditi);

        if(count($price)>0){
            $price = array(array("jumlah"=>"1"));
        }else{
            $price = array(array("jumlah"=>"0"));
        }
        
        if($price){
            $this->response([
                'status' => true,
                'data' => $price
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => false,
                'message' => 'No Data'
            ], REST_Controller::HTTP_NOT_FOUND);
        }
    }

}

?>