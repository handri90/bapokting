<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            redirect('district/Lawang');
        }else{
            $this->load->view('login');
        }
    }

    public function authenticationMember(){
    	$username = $this->input->post('username');
        $password = $this->input->post('password');
       
        $user = $this->Login_model->cek_user_login($username);
        if(isset($user->password)){
            if(password_verify($password, $user->password)){
                $user = $this->Login_model->get_user($username);
                
                // set session user datas
                $_SESSION['user_id']      = $user->idMember;
                $_SESSION['namaMember']     = $user->namaMember;
                $_SESSION['namaHakAkses']     = $user->namaHakAkses;
                $_SESSION['idHakAkses']     = $user->idHakAkses;
                $_SESSION['filename']     = $user->filename;
                $_SESSION['logged_in']    = true;
                
                // user login ok
                redirect('district/Lawang');
            }else{
                $data['error'] = 'Username dan password tidak ditemukan';
            
                $this->load->view('login', $data);
            }
        }else{
            $data['error'] = 'Username dan password tidak ditemukan';
            
            $this->load->view('login', $data);
        }
    }

    public function logout(){
        if (isset($_SESSION['logged_in']) && $_SESSION['logged_in'] === true) {
            
            // remove session datas
            foreach ($_SESSION as $key => $value) {
                unset($_SESSION[$key]);
            }
            
            // user logout ok
            redirect('login');
            
        } else {
            
            redirect('/');
            
        }
    }
}

?>