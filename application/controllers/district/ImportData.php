<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ImportData extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listImportData();
        }else{
            $this->load->view('login');
        }
    }

    public function listImportData(){
        $data['act'] = 'district/ImportData/importDataAct';
            $data['content'] = 'district/ImportData/listimportdata';
	        $this->load->view('district/panel',$data);
    }

    public function importDataAct(){
        $config['upload_path'] = './assets/';
        $config['allowed_types'] = 'xlsx';
        $config['max_size']	= '2048';
        $config['overwrite'] = true;
        $config['file_name'] = $_FILES['userfile']['name'];
    
        $this->upload->initialize($config); // Load konfigurasi uploadnya
        if($this->upload->do_upload()){ // Lakukan upload dan Cek jika proses upload berhasil
        // Jika berhasil :
            // $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
            // print_r($return);

            $datas = $this->upload->data();

            require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');

            $excelreader = new PHPExcel_Reader_Excel2007();
            $loadexcel = $excelreader->load('./assets/'.$datas['raw_name'].".xlsx"); // Load file yang tadi diupload ke folder excel
            $sheet = $loadexcel->getActiveSheet()->toArray(null, true, true ,true);

            $jmlColumn = $loadexcel->getActiveSheet()->getHighestDataColumn();
            $str = array();
            for($i='A';$i<=$jmlColumn;$i++){
                array_push($str,$sheet[1][$i]);
            }

            $kolom = implode(',',$str);
            $data = array();
            $val="";
            for($j=2;$j<=count($sheet);$j++){
                $separator1 = "";
                if($j != count($sheet)){
                    $separator1 = ",";
                }
                $val .= "(";
                for($i='A';$i<=$jmlColumn;$i++){
                    $separator = "";
                    if($i!=$jmlColumn){
                        $separator = ",";
                    }
                    $val .= "'".$sheet[$j][$i]."'".$separator;
                }
                $val .= ")".$separator1;
                
            }
            $namaKolom = $this->ImportData_model->inputDataTable($kolom,$val,$datas['raw_name']);

            redirect('district/ImportData');
            
        }else{
            // Jika gagal :
            $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
            print_r($return);
        }
    }
}

?>