<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ExportData extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listExportData();
        }else{
            $this->load->view('login');
        }
    }

    public function listExportData(){
            $data['listTable'] = $this->ExportData_model->getNamaTable();
            $data['content'] = 'district/ExportData/listexportdata';
	        $this->load->view('district/panel',$data);
    }

    public function getData($namaTable){
        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel.php');
        require(APPPATH.'third_party/PHPExcel-1.8/Classes/PHPExcel/Writer/Excel2007.php');
        
        $objPHPExcel = new PHPExcel();

		$objPHPExcel->getProperties()->setCreator("");
		$objPHPExcel->getProperties()->setLastModifiedBy("");
		$objPHPExcel->getProperties()->setTitle("");
		$objPHPExcel->getProperties()->setSubject("");
		$objPHPExcel->getProperties()->setDescription("");

		$objPHPExcel->setActiveSheetIndex(0);

		$filename = $namaTable.".xlsx";
		
        $objPHPExcel->getActiveSheet()->setTitle("Task-Overview");
        
        $list = $this->ExportData_model->getDataTable($namaTable);
        $namaKolom = $this->ExportData_model->getNameColumn($namaTable);

        $column = "A";
        for($i=0;$i<count($namaKolom);$i++){
            $objPHPExcel->getActiveSheet()->SetCellValue($column."1",$namaKolom[$i]['namakolom']);
            $column++;
        }

        $row=2;
        for($j=0;$j<count($list);$j++){
            $column = "A";
            for($k=0;$k<count($namaKolom);$k++){
                $objPHPExcel->getActiveSheet()->SetCellValue($column.$row,$list[$j][$namaKolom[$k]['namakolom']]);
                $column++;   
            }
            $row++;
        }

        $col = 'A';
		while(true){
			//$objPHPExcel->getActiveSheet()->getStyle($col."8:".$col.$jmlColumn)->applyFromArray($styledata);
			$tempCol = $col++;
			$objPHPExcel->getActiveSheet()->getColumnDimension($tempCol)->setAutoSize(true);
			if($tempCol == $objPHPExcel->getActiveSheet()->getHighestDataColumn()){
				break;
			}
		}
        
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0');

		$writer = PHPExcel_IOFactory::createWriter($objPHPExcel,'Excel2007');
		
		$writer->save('php://output');
		exit;

    }
}

?>