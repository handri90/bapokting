<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class KategoriKomoditi extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listKategoriKomoditi();
        }else{
            $this->load->view('login');
        }
    }

    public function listKategoriKomoditi(){
            $data['list'] = $this->KategoriKomoditi_model->getListKategori();
			$data['content'] = 'district/kategorikomoditi/listkategorikomoditi';
	        $this->load->view('district/panel',$data);
    }

    public function tambahKategoriKomoditi(){
        $data['content'] = 'district/kategorikomoditi/formkategorikomoditi';
        $data['title2'] = 'Tambah Kategori Komoditi';
        $data['act'] = 'district/KategoriKomoditi/tambahKategoriKomoditiAct';
        $this->load->view('district/panel',$data);
    }

    public function tambahKategoriKomoditiAct(){
    	$kategorikomoditi = $this->input->post('namakategorikomoditi');
    	$urutan = $this->input->post('urutan');

        $notifInput = $this->KategoriKomoditi_model->inputKategoriKomoditi($kategorikomoditi,$urutan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/KategoriKomoditi');
    }

    public function deleteKategori($id){
        $notifDelete = $this->KategoriKomoditi_model->deleteKategori($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/KategoriKomoditi');
    }

    public function editkategori($id){
        $data['list'] = $this->KategoriKomoditi_model->getKategori($id);
        $data['content'] = 'district/kategorikomoditi/formkategorikomoditi';
        $data['title2'] = 'Ubah Kategori';
        $data['act'] = 'district/KategoriKomoditi/editkategoriact';
        $this->load->view('district/panel',$data);
    }

    public function editkategoriact(){
        $namakategori = $this->input->post('namakategorikomoditi');
    	$urutan = $this->input->post('urutan');
        $idkategori = $this->input->post('idkategori');

        $notifInput = $this->KategoriKomoditi_model->updateKategoriKomoditi($idkategori,$namakategori,$urutan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/KategoriKomoditi');
    }
}

?>