<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Member extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    public function index()
    {
        if($this->session->userdata('logged_in') == true){
            $this->panelMember();
        }else{
            $this->load->view('login');
        }
    }

    public function panelMember(){
        if($this->session->userdata('idHakAkses') == '2'){
            $data['list'] = $this->Member_model->getListmemberNoSuper();
        }else if($this->session->userdata('idHakAkses') == '3'){
            $data['list'] = $this->Member_model->getListmemberOnlyOneUser($this->session->userdata('user_id'));
        }else{
            $data['list'] = $this->Member_model->getListmember();
        }
        
        $data['content'] = "district/Member/listMember";
        $this->load->view('district/panel',$data);
    }

    public function tambahMember(){
        $data['title'] = 'Tambah User';
        if($this->session->userdata('idHakAkses') == '2'){
            $data['leveluser'] = $this->Member_model->getLevelMemberNoSuper();
        }else if($this->session->userdata('idHakAkses') == '3'){
            $data['leveluser'] = $this->Member_model->getLevelMemberOnlyOne();            
        }else{
            $data['leveluser'] = $this->Member_model->getLevelMember();            
        }
        $data['act'] = 'district/Member/tambahMemberAct';
        $data['content'] = "district/Member/formMember";
        $this->load->view('district/panel',$data);
    }

    public function tambahMemberAct(){
        $data['act'] = 'district/Member/tambahMemberAct';
        $namalengkap = $this->input->post("namalengkap");
        $username = $this->input->post("username");
        $passworduser = $this->input->post("passworduser");
        $leveluser = $this->input->post("leveluser");
        $passworduser = password_hash($passworduser,PASSWORD_BCRYPT,array('cost'=>12));

        $this->upload->initialize($this->set_upload_options());
        $this->upload->do_upload();

        $file_data = $this->upload->data();
        $filename = $file_data['file_name'];

        $notifInput = $this->Member_model->inputMember($namalengkap,$username,$passworduser,$leveluser,$filename);

        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Member');
    }

    private function set_upload_options()
	{   
	    //upload an image options
	    $filename = md5(uniqid(rand(), true));
	    $config = array();
	    $config['upload_path'] = 'upload/fotouser';
	    $config['allowed_types'] = 'jpg|png|jpeg';
	    $config['file_name'] = $filename;

	    return $config;
    }
    
    public function editMember($id){
        $data['list'] = $this->Member_model->getMemberById($id);
        if($this->session->userdata('idHakAkses') == '2'){
            $data['leveluser'] = $this->Member_model->getLevelMemberNoSuper();
        }else if($this->session->userdata('idHakAkses') == '3'){
            $data['leveluser'] = $this->Member_model->getLevelMemberOnlyOne();            
        }else{
            $data['leveluser'] = $this->Member_model->getLevelMember();            
        }
        $data['title'] = 'Ubah User';
        $data['act'] = 'district/Member/ubahMemberAct';
        $data['content'] = "district/Member/formMember";
        $this->load->view('district/panel',$data);
    }

    public function ubahMemberAct(){
        $data['act'] = 'district/Member/ubahMemberAct';
        $namalengkap = $this->input->post("namalengkap");
        $username = $this->input->post("username");
        $passworduser = $this->input->post("passworduser");
        $leveluser = $this->input->post("leveluser");
        $iduser = $this->input->post("iduser");

    	$filename = "";
        if($_FILES['userfile']['name'] != ""){
            $this->upload->initialize($this->set_upload_options());
            $this->upload->do_upload();
            $file_data = $this->upload->data();
            $filename = $file_data['file_name'];
            $this->Member_model->changefoto($filename,$iduser);
        }
    
    	if($iduser == $this->session->userdata('user_id')){
        	$_SESSION['filename'] = $filename;
        }

        $notifInput = $this->Member_model->updateMember($namalengkap,$username,$leveluser,$iduser);

        if(isset($passworduser) && $passworduser != ""){
            $passworduser = password_hash($passworduser,PASSWORD_BCRYPT,array('cost'=>12));
            $this->Member_model->changePasswordMember($passworduser,$iduser);
        }

        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Member');
    }

    public function deleteMember($id){
    	$notifDelete = $this->Member_model->deleteMember($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
		redirect('district/Member');       
    }
}

?>