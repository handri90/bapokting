<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listKecamatan();
        }else{
            $this->load->view('login');
        }
    }

    public function listKecamatan(){
            $data['list'] = $this->Kecamatan_model->getListKecamatan();
			$data['content'] = 'district/Kecamatan/listKecamatan';
	        $this->load->view('district/panel',$data);
    }

    public function tambahKecamatan(){
        $data['content'] = 'district/Kecamatan/formKecamatan';
        $data['title2'] = 'Tambah Kecamatan';
        $data['act'] = 'district/Kecamatan/tambahKecamatanAct';
        //$data['listkategori'] = $this->Kategori_model->getListKategori();
        $this->load->view('district/panel',$data);
    }

    public function tambahKecamatanAct(){
    	$Kecamatan = $this->input->post('namakecamatan');

        $notifInput = $this->Kecamatan_model->inputKecamatan($Kecamatan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Kecamatan');
    }

    public function deleteKecamatan($id){
        $notifDelete = $this->Kecamatan_model->deleteKecamatan($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/Kecamatan');
    }

    public function editKecamatan($id){
    	$data['list'] = $this->Kecamatan_model->getKecamatan($id);
        $data['content'] = 'district/Kecamatan/formKecamatan';
        $data['title2'] = 'Ubah Kecamatan';
        $data['act'] = 'district/Kecamatan/editKecamatanact';
        $this->load->view('district/panel',$data);
    }

    public function editKecamatanact(){
        $Kecamatan = $this->input->post('namakecamatan');
        $idKecamatan = $this->input->post('idkecamatan');

        $notifInput = $this->Kecamatan_model->updateKecamatan($Kecamatan,$idKecamatan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Kecamatan');
    }
}

?>