<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Komoditi extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listKomoditi();
        }else{
            $this->load->view('login');
        }
    }

    public function listKomoditi(){
        if(!empty($_POST)){
            $kategori = $_POST['kategori'];
        }else if(!empty($_SESSION['kategori'])){
            $kategori = $_SESSION['kategori'];
        }else{
            $kategori = 'ALL';
        }

        $_SESSION['kategori'] = $kategori;
        
        $data['search'] = array("kategori"=>$kategori);
        $data['act'] = 'district/Komoditi';
        $data['listkategori'] = $this->KategoriKomoditi_model->getListKategori();
        $data['list'] = $this->Komoditi_model->getListKomoditas($kategori);
        $data['content'] = 'district/komoditi/listkomoditi';
        $this->load->view('district/panel',$data);
    }

    public function tambahKomoditi(){
        $data['content'] = 'district/komoditi/formkomoditi';
        $data['title2'] = 'Tambah Komoditi';
        $data['act'] = 'district/Komoditi/tambahKomoditiAct';
        $data['listkategori'] = $this->KategoriKomoditi_model->getListKategori();
        $this->load->view('district/panel',$data);
    }

    public function tambahKomoditiAct(){
        $idKategori = $this->input->post('idKategoriKomoditi');
    	$namakomoditi = $this->input->post('namakomoditi');

        $notifInput = $this->Komoditi_model->inputKomoditi($idKategori,$namakomoditi);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Komoditi');
    }

    public function deleteKomoditi($id){
        $notifDelete = $this->Komoditi_model->deleteKomodoti($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/Komoditi');
    }

    public function editkomoditi($id){
        $data['list'] = $this->Komoditi_model->getKomoditi($id);
        $data['listkategori'] = $this->KategoriKomoditi_model->getListKategori();
        $data['content'] = 'district/komoditi/formkomoditi';
        $data['title2'] = 'Ubah Komoditi';
        $data['act'] = 'district/Komoditi/editkomoditiact';
        $this->load->view('district/panel',$data);
    }

    public function editkomoditiact(){
        $idKategoriKomoditi = $this->input->post('idKategoriKomoditi');
        $namakomoditi = $this->input->post('namakomoditi');
        $idkomoditi = $this->input->post('idkomoditi');

        $notifInput = $this->Komoditi_model->updateKomoditi($idKategoriKomoditi,$namakomoditi,$idkomoditi);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/Komoditi');
    }
}

?>