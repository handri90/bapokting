<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Satuan extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listSatuan();
        }else{
            $this->load->view('login');
        }
    }

    public function listSatuan(){
        // $id = $this->session->userdata('user_id');
        // if($this->session->userdata('idHakAkses') != 3){
        //     $data['listharga'] = $this->Harga_model->getListHarga();
        // }else{
        //     $data['listharga'] = $this->Harga_model->getListHarga($id);
        // }
            $data['list'] = $this->Satuan_model->getListSatuan();
			$data['content'] = 'district/satuan/listsatuan';
	        $this->load->view('district/panel',$data);
    }

    public function tambahSatuan(){
        $data['content'] = 'district/satuan/formsatuan';
        $data['title2'] = 'Tambah Satuan';
        $data['act'] = 'district/Satuan/tambahSatuanAct';
        //$data['listkategori'] = $this->Kategori_model->getListKategori();
        $this->load->view('district/panel',$data);
    }

    public function tambahSatuanAct(){
    	$satuan = $this->input->post('namasatuan');

        $notifInput = $this->Satuan_model->inputSatuan($satuan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/satuan');
    }

    public function deleteSatuan($id){
        $notifDelete = $this->Satuan_model->deleteSatuan($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/satuan');
    }

    public function editsatuan($id){
    	$data['list'] = $this->Satuan_model->getSatuan($id);
        $data['content'] = 'district/satuan/formsatuan';
        $data['title2'] = 'Ubah Satuan';
        $data['act'] = 'district/Satuan/editsatuanact';
        $this->load->view('district/panel',$data);
    }

    public function editsatuanact(){
        $satuan = $this->input->post('namasatuan');
        $idsatuan = $this->input->post('idsatuan');

        $notifInput = $this->Satuan_model->updateSatuan($satuan,$idsatuan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/satuan');
    }
}

?>