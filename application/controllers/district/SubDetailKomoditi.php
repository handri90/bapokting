<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class SubDetailKomoditi extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listSubDetailKomoditi();
        }else{
            $this->load->view('login');
        }
    }

    public function listSubDetailKomoditi(){
            if(!empty($_POST)){
                $kategori = $_POST['kategori'];
                $komoditas = empty($_POST['komoditas'])?'ALL':$_POST['komoditas'];
            }else if(!empty($_SESSION['kategori'])){
                $kategori = $_SESSION['kategori'];
                $komoditas = empty($_SESSION['komoditas'])?'ALL':$_SESSION['komoditas'];
            }else{
                $kategori = 'ALL';
                $komoditas = 'ALL';
            }

            $_SESSION['kategori'] = $kategori;
            $_SESSION['komoditas'] = $komoditas;
            
            $data['search'] = array("kategori"=>$kategori,"komoditas"=>$komoditas);
            $data['act'] = 'district/SubDetailKomoditi';
            $data['listkategori'] = $this->KategoriKomoditi_model->getListKategori();
            $data['listkomoditi'] = $this->SubDetailKomoditi_model->getKomoditasByKategori($kategori);
            $data['list'] = $this->SubDetailKomoditi_model->getListSubDetailKomoditi2($kategori,$komoditas);
			$data['content'] = 'district/subdetailkomoditi/listsubdetailkomoditi';
	        $this->load->view('district/panel',$data);
    }

    public function tambahSubDetailKomoditi(){
        $data['content'] = 'district/subdetailkomoditi/formsubdetailkomoditi';
        $data['title2'] = 'Tambah Sub Detail Komoditi';
        $data['act'] = 'district/SubDetailKomoditi/tambahSubDetailKomoditiAct';
        $data['listkomoditi'] = $this->SubDetailKomoditi_model->getListKomoditi();
        $data['listsatuan'] = $this->Satuan_model->getListSatuan();
        $this->load->view('district/panel',$data);
    }

    public function tambahSubDetailKomoditiAct(){
    	$komoditi = $this->input->post('idkomoditi');
        $namasubdetailkomoditi = $this->input->post('namabarang');
        $satuan = $this->input->post('satuan');

        $notifInput = $this->SubDetailKomoditi_model->inputHargaKomoditi($komoditi,$namasubdetailkomoditi,$satuan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        
        redirect('district/SubDetailKomoditi');
    }

    public function deleteSubKomoditi($id){
        $notifDelete = $this->SubDetailKomoditi_model->deleteSubKomoditi($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/SubDetailKomoditi');
    }

    public function editsubkomoditi($id){
        $data['list'] = $this->SubDetailKomoditi_model->getSubKomoditi($id);
        $data['listkomoditi'] = $this->SubDetailKomoditi_model->getListKomoditi();
        $data['listsatuan'] = $this->Satuan_model->getListSatuan();
        $data['content'] = 'district/subdetailkomoditi/formsubdetailkomoditi';
        $data['title2'] = 'Ubah Sub Detail Komoditi';
        $data['act'] = 'district/SubDetailKomoditi/editsubdetailkomoditiact';
        $this->load->view('district/panel',$data);
    }

    public function editsubdetailkomoditiact(){
        $komoditi = $this->input->post('idkomoditi');
        $namasubdetailkomoditi = $this->input->post('namabarang');
        $satuan = $this->input->post('satuan');
        $idsubkomoditi = $this->input->post('idsubkomoditi');

        $notifInput = $this->SubDetailKomoditi_model->updateSubKomoditi($komoditi,$namasubdetailkomoditi,$satuan,$idsubkomoditi);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/SubDetailKomoditi');
    }

    public function getKomoditas(){
        $val = $this->input->post("val");
        $data = $this->SubDetailKomoditi_model->getKomoditasByKategori($val);
        $option = "";
        $option .= "<option value='ALL'>-- Semua Komoditi --</option>";
        for($i=0;$i<count($data);$i++){
            $option .= "<option value='".$data[$i]['idKomoditi']."'>".$data[$i]['namaKomoditi']."</option>";
        }
        echo $option;
    }
}

?>