<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasar extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listPasar();
        }else{
            $this->load->view('login');
        }
    }

    public function listPasar(){
            $data['list'] = $this->Pasar_model->getListPasar();
			$data['content'] = 'district/pasar/listpasar';
	        $this->load->view('district/panel',$data);
    }

    public function tambahPasar(){
        $data['content'] = 'district/pasar/formpasar';
        $data['title2'] = 'Tambah Pasar';
        $data['act'] = 'district/Pasar/tambahPasarAct';
        $data['listkecamatan'] = $this->Kecamatan_model->getListKecamatan();
        $this->load->view('district/panel',$data);
    }

    public function tambahPasarAct(){
    	$pasar = $this->input->post('namapasar');
        $idKecamatan = $this->input->post('kecamatan');

        $notifInput = $this->Pasar_model->inputPasar($pasar,$idKecamatan);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/pasar');
    }

    public function deletePasar($id){
        $notifDelete = $this->Pasar_model->deletePasar($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/pasar');
    }

    public function editpasar($id){
        $data['list'] = $this->Pasar_model->getPasar($id);
        $data['listkecamatan'] = $this->Kecamatan_model->getListKecamatan();
        $data['content'] = 'district/pasar/formpasar';
        $data['title2'] = 'Ubah Pasar';
        $data['act'] = 'district/Pasar/editpasaract';
        $this->load->view('district/panel',$data);
    }

    public function editpasaract(){
        $kecamatan = $this->input->post('kecamatan');
        $pasar = $this->input->post('namapasar');
        $idpasar = $this->input->post('idpasar');

        $notifInput = $this->Pasar_model->updatePasar($kecamatan,$pasar,$idpasar);
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
    	
		redirect('district/pasar');
    }
}

?>