<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class HargaKomoditi extends CI_Controller {

	public function __construct()
        {
                parent::__construct();
        }

    
    public function index()
    {
    	if($this->session->userdata('logged_in') == true){
            $this->listHargaKomoditi();
        }else{
            $this->load->view('login');
        }
    }

    public function listHargaKomoditi(){
            if(!empty($_POST)){
                $pasar = $_POST['pasar'];
                $komoditi = $_POST['komoditi'];
                $tanggalharga = $_POST['tanggalharga'];
            }else if(!empty($_SESSION['search_pasar'])){
                $pasar = $_SESSION['search_pasar'];
                $komoditi = $_SESSION['search_komoditi'];
                $tanggalharga = $_SESSION['search_tanggal'];
            }else{
                $pasar = 'ALL';
                $komoditi = 'ALL';
                $tanggalharga = date("d-m-Y");
            }

            $_SESSION['search_pasar'] = $pasar;
            $_SESSION['search_komoditi'] = $komoditi;
            $_SESSION['search_tanggal'] = $tanggalharga;
            
            $data['search'] = array("pasar"=>$pasar,"komoditi"=>$komoditi,"tanggal"=>$tanggalharga);
            $data['act'] = 'district/HargaKomoditi/listHargaKomoditi';
            if(!in_array($this->session->userdata('idHakAkses'),array('3'))){
                $data['list'] = $this->HargaKomoditi_model->getListHargaKomoditi($pasar,$komoditi,$tanggalharga);
            }else{
                $data['list'] = $this->HargaKomoditi_model->getListHargaKomoditiByUser($pasar,$komoditi,$tanggalharga,$this->session->userdata('user_id'));
            }
            $data['listpasar'] = $this->Pasar_model->getListPasar();
            $data['listKomoditi'] = $this->Komoditi_model->getListKomoditi();
			$data['content'] = 'district/hargakomoditi/listhargakomoditi';
	        $this->load->view('district/panel',$data);
    }

    public function tambahHargaKomoditi(){
        $data['content'] = 'district/hargakomoditi/formhargakomoditi';
        $data['title2'] = 'Tambah Harga Komoditi';
        $data['act'] = 'district/HargaKomoditi/tambahHargaKomoditiAct';
        $data['listkategorikomoditi'] = $this->Komoditi_model->getListKategoriKomoditiFormHarga();
        $data['listsatuan'] = $this->Satuan_model->getListSatuan();
        $data['listpasar'] = $this->Pasar_model->getListPasar();
        $this->load->view('district/panel',$data);
    }

    public function tambahHargaKomoditiAct(){

        $memberId = $this->session->userdata('user_id');
        $pasar = $_POST['pasar'];
        $tanggalharga = $_POST['tanggalharga'];
        $_SESSION['search_tanggal'] = $tanggalharga;
        $komoditi = $_POST['komoditi'];
        $harga = $_POST['harga'];

        for($i = 0;$i<count($komoditi);$i++){
            $notifInput = $this->HargaKomoditi_model->inputHargaKomoditi($pasar,$tanggalharga,$komoditi[$i],$harga[$i],$memberId);
        }
        if($notifInput){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }

        redirect('district/HargaKomoditi');
    }

    public function editHargaKomoditi($id){
        $data['content'] = 'district/hargakomoditi/formhargakomoditi';
        $data['title2'] = 'Ubah Harga Komoditi';
        $data['act'] = 'district/HargaKomoditi/ubahHargaKomoditiAct';
        $data['listkategorikomoditi'] = $this->Komoditi_model->getListKategoriKomoditiFormHarga();
        $data['listpasar'] = $this->Pasar_model->getListPasar();
        $data['detailharga'] = $this->HargaKomoditi_model->getDetailHargaById($id);
        $this->load->view('district/panel',$data);
    }

    public function ubahHargaKomoditiAct(){
        $pasar = $_POST['pasar'];
        $tanggalharga = $_POST['tanggalharga'];
        $komoditi = $_POST['komoditi'];
        $harga = $_POST['harga'];
        $idDetail = $_POST['idDetail'];
        $notifUpdate = $this->HargaKomoditi_model->updateHargaKomoditi($pasar,$tanggalharga,$komoditi[0],$harga[0],$idDetail);
        if($notifUpdate){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }

        redirect('district/HargaKomoditi');
    }

    public function deleteHargaKomoditi($id){
        $notifDelete = $this->HargaKomoditi_model->deleteDetailHarga($id);
        if($notifDelete){
            $this->session->set_flashdata('message', 'Success');
        }else{
            $this->session->set_flashdata('message', 'Gagal');
        }
        redirect('district/HargaKomoditi');
    }

    public function getKomoditiByMarketanDate(){
        $market = $this->input->post('vValMarket');
        $tanggal = $this->input->post('vValDate');
        $detailid = $this->input->post('detailid');
        $komoditiidsaved = $this->input->post('komoditiidsaved');
        $komoditi = $this->Komoditi_model->getListKategoriKomoditiFormHargaNew($market,$tanggal,$detailid,$komoditiidsaved);
        $html = "";
        for($i=0;$i<count($komoditi);$i++){
            $explNamaKomoditi = explode('|', $komoditi[$i]['namasubdetailkomoditi']);
            $explIdKomoditi = explode('|', $komoditi[$i]['idsubdetailkomoditi']);
            $explSatuan = explode('|', $komoditi[$i]['satuan']);
            $html .= '<optgroup label="'.$komoditi[$i]['labeloptgroup'].'">';
            for($k=0;$k<count($explNamaKomoditi);$k++){
                $selected = "";
                if($explIdKomoditi[$k] == $komoditiidsaved){
                    $selected ="selected";
                }else{
                    $selected = "";
                }
                $html .= '<option '.$selected.' value="'.$explIdKomoditi[$k].'">'.$explNamaKomoditi[$k].' / '.$explSatuan[$k].'</option>';
            }
            $html .= '</optgroup>';
        }
        echo $html;
    }

    public function cekHargaKomoditiPerubahanValid(){
        $market = $this->input->post('market');
        $tanggal = $this->input->post('tanggalPasar');
        $detailid = $this->input->post('detailid');
        $komoditiId = $this->input->post('komoditiId');
        $cek = $this->HargaKomoditi_model->cekValidData($market,$tanggal,$detailid,$komoditiId);
        if(count($cek) > 0){
            echo 'fail';
        }else{
            echo 'good';
        }
    }
}

?>