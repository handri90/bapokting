<section class="blog_area">
            <div class="container">
            <div class="header-table-page header-table-new"><h3>Tabel Harga Barang Pokok dan Barang Penting</h3></div>
                <div class="row row-new">
                    <div class="col-lg-3">
                        <div class="blog_right_sidebar">
                            <form action="<?php echo base_url().'Frontier/tabelHarga'; ?>" method="POST">
                                <aside class="single_sidebar_widget ads_widget">
                                    <label class="tagsearch">Komoditas</label>
                                    <select name="komoditi" id="" class="form-control komoditaschoice" size="10">
                                        <?php
                                        for($i=0;$i<count($listKomoditiSearch);$i++){
                                            $subDetailId = explode('|',$listKomoditiSearch[$i]['idSubDetail']);
                                            $namaSubKomoditi = explode('|',$listKomoditiSearch[$i]['namaSubKomoditi']);
                                            $selected = "";
                                            if($listKomoditiSearch[$i]['idKomoditi'] == $cacheform['idKomoditi']){
                                                $selected = "selected";
                                            }
                                            ?>
                                            <option <?php echo $selected; ?> value="<?php echo $listKomoditiSearch[$i]['idKomoditi']; ?>"><?php echo $listKomoditiSearch[$i]['namaKomoditi']; ?></option>
                                            <?php
                                            for($k=0;$k<count($subDetailId);$k++){
                                                $selected = "";
                                                if($subDetailId[$k] == $cacheform['idKomoditi']){
                                                    $selected = "selected";
                                                }
                                                ?>
                                                <option <?php echo $selected; ?> value="<?php echo $subDetailId[$k]; ?>">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $namaSubKomoditi[$k] ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                    <div class="breakline"></div>
                                </aside>
                                <aside class="single_sidebar_widget ads_widget">
                                    <label class="tagsearch">Kecamatan</label>
                                    <select name="kecamatan" id="" class="form-control kecamatanchoice spinsearch" size="6">
                                    <?php
                                        foreach($listkecamatan as $key){
                                            $selected = "";
                                            if($key['idKecamatan'] == $cacheform['idKec']){
                                                $selected = "selected";
                                            }
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $key['idKecamatan']; ?>"><?php echo $key['namaKecamatan']; ?></option>
                                        <?php
                                        }
                                        ?>
                                    </select>
                                <div class="breakline"></div>
                                </aside>
                                <aside class="single_sidebar_widget ads_widget">
                                    <label class="tagsearch">Pasar</label>
                                    <select name="pasar" id="" class="form-control pasarfrsearch spinsearch" size="6">
                                    <?php
                                    if(!empty($listpasar)){
                                        foreach($listpasar as $key){
                                            $selected = "";
                                            if($key['idPasar'] == $cacheform['idPasar']){
                                                $selected = "selected";
                                            }
                                        ?>
                                        <option <?php echo $selected; ?> value="<?php echo $key['idPasar']; ?>"><?php echo $key['namaPasar']; ?></option>
                                        <?php
                                        }
                                    }
                                        ?>
                                    </select>
                                <div class="breakline"></div>
                                </aside>
                                <aside class="single_sidebar_widget ads_widget">
                                    <label class="tagsearch">Tipe Laporan</label>
                                    <select name="tipeLaporan" class="form-control tipeLaporan formatlaporan">
                                        <option <?php echo $cacheform['tipelaporan'] == 'tipelaporan1'?'selected':''; ?> value="tipelaporan1">Laporan Harian</option>
                                        <option <?php echo $cacheform['tipelaporan'] == 'tipelaporan2'?'selected':''; ?> value="tipelaporan2">Laporan Day to Day</option>
                                        <option <?php echo $cacheform['tipelaporan'] == 'tipelaporan3'?'selected':''; ?> value="tipelaporan3">Laporan Bulanan</option>
                                    </select>
                                    <div class="breakline"></div>
                                </aside>
                                <aside class="single_sidebar_widget ads_widget">
                                    <label class="tagsearch">Tanggal Mulai</label>
                                    <input type="text" class="form-control has-feedback-left" id="single_cal11"  aria-describedby="inputSuccess2Status" name="tanggalhargamulai" value="<?php echo !empty($cacheform['tanggalmulai'])?$cacheform['tanggalmulai']:''; ?>">
                                    <div class="breakline"></div>
                                <div class="breakline"></div>
                                </aside>
                                <aside class="single_sidebar_widget ads_widget">
                                    <label class="tagsearch">Tanggal Selesai</label>
                                    <input type="text" class="form-control has-feedback-left" id="single_cal12"  aria-describedby="inputSuccess2Status" name="tanggalhargaselesai" value="<?php echo !empty($cacheform['tanggalselesai'])?$cacheform['tanggalselesai']:''; ?>">
                                    <div class="breakline"></div>
                                </aside>
                                <aside class="single_sidebar_widget ads_widget">
                                    <button class="tombollaporan" type="submit">Lihat Laporan</button>
                                </aside>
                                <!-- <aside class="single_sidebar_widget ads_widget">
                                    <label class="tagsearch">Format Laporan</label>
                                    <select class="form-control ">
                                        <option value="laporan1">Format Laporan 1</option>
                                        <option value="laporan2">Format Laporan 2</option>
                                        <option value="laporan4">Format Laporan 3</option>
                                    </select>
                                    <div class="breakline"></div>
                                </aside> -->
                                <aside class="side-cetaklaporan">
                                	<button class="tombollaporan generateExcel" type="button">Cetak Laporan</button>
<!--                                     <a class="cetaklaporan generateExcel" href="">Cetak Laporan</a> -->
                                </aside>
                            </form>
                        </div>
                    </div>    
                <div class="col-lg-9">
                        <div class="blog_left_sidebar">
                            <div class="tab-price-header">
                                <div class="title-header">Perkembangan Harga Barang Pokok dan Barang Penting</div>
                                <div><span>Periode</span>: <?php echo date_indo_new($cacheform['tanggalmulai'])." - ".date_indo_new($cacheform['tanggalselesai']); ?></div>
                                <div><span>Kabupaten/Kota</span>: Kotawaringin Barat</div>
                                <div><span>Kecamatan</span>: <?php echo $labelinfokecamatan; ?></div>
                                <div><span>Tipe Laporan</span>: <?php echo $labeltipelaporan; ?></div>
                            </div>
                          <div class="buttonaksi">
                            <div class="kakiri">< Kiri</div>
                            <div class="kakanan">Kanan ></div>
                          </div>
                          <div class="table-responsive">
                        <table class="table table-striped custom-table">
                          <thead>
                            <?php echo $generatetablehead; ?>
                          </thead>
                          <tbody>
                          <?php echo $generatetablebody; ?>
                          </tbody>
                        </table>
                                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>