<section class="banner_areaku">
            <div class="booking_table d_flex align-items-center">
            	<div class="overlay bg-parallax" data-stellar-ratio="0.9" data-stellar-vertical-offset="0" data-background=""></div>
				
            </div>
            <div class="hotel_booking_area position">
                <div class="container">
                    <div class="hotel_booking_table">
                        <div class="col-md-12">
                            <div class="head-info-frontier">Harga Rata-Rata dan Perubahan</div>
                            <div class="boking_table">
                                <div class="row">
                                    <div class="col-md-2">
                                        <div class="book_tabel_item kecamatanLeft">
                                            <label class="label-search-front">Tanggal</label>
                                            <input type="text" class="form-control has-feedback-left-front tanggalsearchfront" id="single_cal11"  aria-describedby="inputSuccess2Status" value="<?php echo !empty($cacheform['tanggalsearchfront'])?$cacheform['tanggalsearchfront']:date('d-m-Y'); ?>">
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="book_tabel_item kecamatanLeft">
                                            <label class="label-search-front">Kecamatan</label>
                                            <div class="input-group">
                                                <select id="useNice" class="wide actKec">
                                                    <option data-display="Semua Kecamatan" value="ALL">-- Semua Kecamatan --</option>
                                                    <?php
                                                        foreach($listkecamatan as $key){
                                                      ?>
                                                        <option value="<?php echo $key['idKecamatan']; ?>"><?php echo $key['namaKecamatan']; ?></option>
                                                      <?php
                                                        }
                                                      ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="book_tabel_item">
                                            <label class="label-search-front">Kategori</label>
                                            <div class="input-group">
                                                <select id="kategoriNice" class="wide actKat">
                                                    <option data-display="Semua Kategori" value="ALL">-- Semua Kategori --</option>
                                                    <?php
                                                        foreach($listkategorikomoditi as $key){
                                                      ?>
                                                        <option value="<?php echo $key['idKategoriKomoditi']; ?>"><?php echo $key['namaKategoriKomoditi']; ?></option>
                                                      <?php
                                                        }
                                                      ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="arrow">
                                            <i class="fa fa-chevron-up actArrow upArrow" style="cursor: pointer"></i>
                                            <i class="fa fa-chevron-down actArrow downArrow" style="cursor: pointer"></i>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mb_30 distrik3">
                    </div>
                    
                </div>
            </div>
</section>

<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"></h4>
        </div>
        <div class="modal-body">
          <table id="example" class="stripe cell-border" width="100%">
            <thead>
                <tr>
                    <th>&nbsp;</th>
                    <th class="titlestatus column1">24 Feb</th>
                    <th class="titlestatus column2">25 Feb</th>
                    <th class="titlestatus column3">26 Feb</th>
                    <th class="titlestatus column4">27 Feb</th>
                    <th class="titlestatus column5">28 Feb</th>
                </tr>
                <tr>
                  <td class="avgall">Semua Kecamatan</td>
                  <td class="avgall">0</td>
                  <td class="avgall">0</td>
                  <td class="avgall">0</td>
                  <td class="avgall">0</td>
                  <td class="avgall">0</td>
              </tr>
            </thead>
          </table>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>