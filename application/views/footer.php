       <!--================ start footer Area  =================-->	
        <footer class="footer-area section_gap">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12  col-md-12 col-sm-12">
                        <div class="single-footer-widget">
                            <img src="<?php echo base_url('assets/image/logokobar.png'); ?>" width="70" />
                        <p>Dinas Perindustrian, Perdagangan, Koperasi, Usaha Kecil dan Menengah Kabupaten Kotawaringin Barat</p>
                        <p><i class="fa fa-map-marker"></i>Jalan Sutan Syahrir Nomor 48, Pangkalan Bun</p>
                            <!-- <p>Pusat Informasi Harga Bahan Pokok dan Barang Penting Kabupaten Kotawaringin Barat</p> -->
                        </div>
                    </div>
                    <!-- <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Navigation Links</h6>
                            <div class="row">
                                <div class="col-4">
                                    <ul class="list_style">
                                        <li><a href="#">Home</a></li>
                                        <li><a href="#">Feature</a></li>
                                        <li><a href="#">Services</a></li>
                                        <li><a href="#">Portfolio</a></li>
                                    </ul>
                                </div>
                                <div class="col-4">
                                    <ul class="list_style">
                                        <li><a href="#">Team</a></li>
                                        <li><a href="#">Pricing</a></li>
                                        <li><a href="#">Blog</a></li>
                                        <li><a href="#">Contact</a></li>
                                    </ul>
                                </div>										
                            </div>							
                        </div>
                    </div>							
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget">
                            <h6 class="footer_title">Newsletter</h6>
                            <p>For business professionals caught between high OEM price and mediocre print and graphic output, </p>		
                            <div id="mc_embed_signup">
                                <form target="_blank" action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01" method="get" class="subscribe_form relative">
                                    <div class="input-group d-flex flex-row">
                                        <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Email Address '" required="" type="email">
                                        <button class="btn sub-btn"><span class="lnr lnr-location"></span></button>		
                                    </div>									
                                    <div class="mt-10 info"></div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6">
                        <div class="single-footer-widget instafeed">
                            <h6 class="footer_title">InstaFeed</h6>
                            <ul class="list_style instafeed d-flex flex-wrap">
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-01.jpg" alt=""></li>
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-02.jpg" alt=""></li>
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-03.jpg" alt=""></li>
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-04.jpg" alt=""></li>
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-05.jpg" alt=""></li>
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-06.jpg" alt=""></li>
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-07.jpg" alt=""></li>
                                <li><img src="<?php echo base_url(); ?>assets/image/instagram/Image-08.jpg" alt=""></li>
                            </ul>
                        </div>
                    </div>						 -->
                </div>
<!--                 <div class="border_line"></div>
                <div class="row footer-bottom d-flex justify-content-between align-items-center">
                    <p class="col-lg-8 col-sm-12 footer-text m-0">Pusat Informasi Harga Barang Pokok dan Barang Penting Kabupaten Kotawaringin Barat</p>
                    <div class="col-lg-4 col-sm-12 footer-social">
                        <a href="#"><i class="fa fa-facebook"></i></a>
                        <a href="#"><i class="fa fa-twitter"></i></a>
                        <a href="#"><i class="fa fa-dribbble"></i></a>
                        <a href="#"><i class="fa fa-behance"></i></a>
                    </div>
                </div> -->
            </div>
        </footer>
		<!--================ End footer Area  =================-->
        
        
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="<?php echo base_url(); ?>assets/js/jquery-3.2.1.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-1.7.2.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.sparkline.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/popper.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendors/owl-carousel/owl.carousel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.ajaxchimp.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendors-admin/vendors/moment/min/moment.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendors-admin/vendors/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendors/nice-select/js/jquery.nice-select.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/mail-script.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/stellar.js"></script>
        <script src="<?php echo base_url(); ?>assets/vendors/lightbox/simpleLightbox.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/custom.js"></script>
        <script type="text/javascript" charset="utf8" src="<?php echo base_url(); ?>assets/js/jquery.dataTables.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function(){
            	if(navigator.userAgent.match(/Android/i) || navigator.userAgent.match(/webOS/i) || navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPad/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/BlackBerry/i) || navigator.userAgent.match(/Windows Phone/i)){
    				$("body").html("");
                	let html = "<div class='load-ps'><img width='120' src='<?php echo base_url(); ?>assets/image/icon.png' /><p class='title-mob-dev'>Bapokting Kobar</p><p>Informasi perkembangan harga komoditas di Kabupaten Kobar.</p><a target='_blank' href='https://play.google.com/store/apps/details?id=com.disperindagkopukm.bapoktingkobar'><img width='120' src='<?php echo base_url(); ?>assets/image/google-play-store.png' /></a><img class='phone-mag' width='140' src='<?php echo base_url(); ?>assets/image/phone.png' /></div>";
                	$("body").html(html);
 				}
            
                setSparkline();
                function setSparkline() {
                    $('.inlinebar.raw').sparkline('html', {
                        type: 'line',
                        width: '100',
                        height: '70',
                        lineColor: '#ffffff',
                        fillColor: '#0f334e',
                        lineWidth: 2,
                        spotColor: '#ffffff',
                        minSpotColor: '#27ae60',
                        maxSpotColor: '#c0392b',
                        highlightSpotColor: '#ffffff',
                        highlightLineColor: '#ffffff',
                        spotRadius: 5,
                        tooltipOffsetX:10,
                        tooltipOffsetY:10
                    }).removeClass('raw');
                }

                function getDetailHargaPasar(id,kat,datesearch){
                    $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>/Frontier/GetDataHargaPasar", 
                            data:{id:id,kat:kat,datesearch:datesearch},
                            dataType : 'json',
                            async : true,
                            success: 
                            function(data){
                            var i;
                            var htmlz='<div class="listArrow">';
                            for(i=0; i<data.length; i++){
                                htmlz += '<div class="col-lg-3 col-md-8 langAnchor ruleHeight"><a class="popUpDetail" idKomoditas="'+data[i].idSubDetailKomoditi+'"><div class="facilities_item"><div class="title-report">'+data[i].namakomoditi+'</div><div class="desc-avg"><div class="avg-now">'+data[i].hargaavg1+'<div class="satuan">Per '+data[i].namaSatuan+'</div></div><div class="persentase '+data[i].classarrow+'">'+data[i].arrowpersentase+'<span>'+data[i].persentase+'% - Rp'+data[i].rangePrice+'</span></div></div><div class="inlinebar raw">'+data[i].node1+'</div><div class="clear"></div></div></a></div>';
                            }
                            htmlz += "</div>";
                            $(".distrik3").html(htmlz);
                            setSparkline();
                            }
                    });
                }

                $(".tanggalsearchfront").change(function(){
                    let datesearch = $(this).val();
                    getDetailHargaPasar("ALL","ALL",datesearch);
                });

                $(".actKec").change(function() {
                    let id = (this.value);
                    let kat = $(".actKat").val();
                	let datesearch = $(".tanggalsearchfront").val();
                    getDetailHargaPasar(id,kat,datesearch);
                });

                $(".actKat").change(function() {
                    let kat = (this.value);
                    let id = $(".actKec").val();
                	let datesearch = $(".tanggalsearchfront").val();
                    getDetailHargaPasar(id,kat,datesearch);
                });

                $(".actArrow").click(function(){
                    let list = $('.listArrow'),
                    height = list.height();
                    let up = $(this).is('.upArrow');
                    let position = list.scrollTop();
                    if (up) {
                        list.animate({'scrollTop': '-=' + height});
                    } else {
                        list.animate({'scrollTop': '+=' + height});        
                    }
                });

                let tableku = $('#example').DataTable({
                    "scrollX": "100%",
                    "scrollY": "310px",
                    "scrollCollapse": true,
                    "sorting":false,
                    "paging":false,
                    "filter":false,
                    "info":false,
                    "autoWidth":false,
                    "columns":[
                        {"class": "text-left date-col", "width": "45%"},
                        {"class": "text-right date-col column1", "width": "11%"},
                        {"class": "text-right date-col column2", "width": "11%"},
                        {"class": "text-right date-col column3", "width": "11%"},
                        {"class": "text-right date-col column4", "width": "11%"},
                        {"class": "text-right date-col column5", "width": "11%"}
                    ],
                    "rowCallback": function(row, data, index) {
                        let rowClass = data[data.length - 2];
                        let rowValue = data[data.length - 1];

                        $(row).addClass(rowClass);
                        $(row).attr('value', rowValue);

                        if(rowClass == 'kecamatan') {
                            $('td:first-child', $(row)).css('cursor', 'pointer');
                        }
                    }
                });

                $('body').on('click','.popUpDetail',function(){
                	let tanggal = $('.tanggalsearchfront').val();
                    let kecname = $('.actKec option:selected').html();
                    let idKec = $('.actKec option:selected').val();
                    let komId = $(this).attr('idKomoditas');
                    let komTitle = $('.title-report', this).html();
                    $('#myModal').modal('show');
                    $(".modal-title").html(komTitle);
                    tableku.clear();

                    $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>/Frontier/getDetailHargaKomoditi", 
                            data:{komId:komId,idKec:idKec,tanggal:tanggal},
                            dataType : 'json',
                            async : true,
                            success: 
                            function(data){
                                setTimeout(function(){
                                    jQuery.each(data.price, function(c, prices){
                                        tableku.row.add(prices).draw();
                                    });
                                    
                                    jQuery.each(data.datetittle, function(k, date){
                                        $('.titlestatus').eq(k).html(date.lima);
                                    });

                                    jQuery.each(data.avgall, function(k, all){
                                        $('.avgall').eq(k).html(all.satu);
                                    });

                                    jQuery('#example .kecamatan td:first-child').click(function() {
                                        var v = jQuery(this).parent().val();
                                        jQuery('.fa', jQuery(this)).toggleClass('fa-minus-circle');
                                        jQuery('.fa', jQuery(this)).toggleClass('fa-plus-circle');

                                        if(jQuery('.fa', jQuery(this)).hasClass('fa-plus-circle')) {
                                            jQuery('.fa', jQuery(this)).css('color', 'green');
                                            jQuery('#example .pasar-'+v).hide();
                                        } else {
                                            jQuery('.fa', jQuery(this)).css('color', 'red');
                                            jQuery('#example .pasar-'+v).show();
                                        }
                                    });
                                },200);
                            }
                    });
                });

                $('#single_cal1').daterangepicker({
                    singleDatePicker: true,
                    singleClasses: "picker_1",
                    locale: {format: 'DD-MM-YYYY'}
                    }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });

                // $(".custom-table td:nth-child(3),.custom-table td:nth-child(4)").css("color","black");
                let page = 0;
                let fixedKolom = 2;
            	let tlaporan = $(".formatlaporan").val();
            	let kolomBody = 0;
            	if(tlaporan == 'tipelaporan3'){
                   kolomBody = 5;
                }else{
                	kolomBody = 6;
                }
                let jumlahKolom = $(".custom-table th").length;
                hideKolom();
                function hideKolom(){
                    let mulaikolom = (page*kolomBody)+fixedKolom;
                    let akhirKolom = mulaikolom+kolomBody;
                    if(akhirKolom > jumlahKolom) {
                        mulaikolom = jumlahKolom - kolomBody;
                        akhirKolom = jumlahKolom;
                        page--;
                    }
                    if(mulaikolom < 1) {
                        mulaikolom = 1;
                        akhirKolom = kolomBody + fixedKolom;
                        page++;
                    }
                    $('.custom-table td, .custom-table th').hide();
                    for(let i=1;i<=fixedKolom;i++){
                        $(".custom-table th:nth-child("+i+"),.custom-table td:nth-child("+i+")").show();
                    }
                    //body table
                    
                    for(let i=(mulaikolom+1);i<=akhirKolom;i++){
                        $(".custom-table th:nth-child("+i+"),.custom-table td:nth-child("+i+")").show();
                    }

                }

                $(".kakiri").click(function(){
                    page--;
                    hideKolom();
                });

                $(".kakanan").click(function(){
                    page++;
                    hideKolom();
                });

                $('#single_cal11').daterangepicker({
                    maxDate: new Date(),
                    singleDatePicker: true,
                    singleClasses: "picker_1",
                    startDate : $("#single_cal11").val(),
                    locale: {format: 'DD-MM-YYYY'}
                }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });

                $('#single_cal12').daterangepicker({
                    singleDatePicker: true,
                    singleClasses: "picker_1",
                    startDate : $("#single_cal12").val(),
                    maxDate: new Date(),
                    locale: {format: 'DD-MM-YYYY'}
                }, function(start, end, label) {
                    console.log(start.toISOString(), end.toISOString(), label);
                });

                $(".generateExcel").click(function(){
                    let komoditi = $(".komoditaschoice").val() == null?'ALL':$(".komoditaschoice").val();
                    let kecamatan = $(".kecamatanchoice").val() == null?'ALL':$(".kecamatanchoice").val();
                    let pasar = $(".pasarfrsearch").val() == null?'ALL':$(".pasarfrsearch").val();
                    let tgglMulai = $("#single_cal11").val() == null?'ALL':$("#single_cal11").val();
                    let tgglSelesai = $("#single_cal12").val() == null?'ALL':$("#single_cal12").val();
                    let formatlaporan = $(".formatlaporan").val() == null?'ALL':$(".formatlaporan").val();
                    window.open("<?php echo base_url(); ?>/Frontier/generateDownload?komoditi="+komoditi+"&kecamatan="+kecamatan+"&tgglMulai="+tgglMulai+"&tgglSelesai="+tgglSelesai+"&formatlaporan="+formatlaporan+"&pasar="+pasar);
                });

                $(".kecamatanchoice").change(function(){
                    let id = $(this).val();
                    $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>Frontier/GetListPasar", 
                            data:{id:id},
                            success: 
                            function(data){
                                $(".pasarfrsearch").html(data);
                            }
                    });
                });
            });

        </script>
    </body>
</html>