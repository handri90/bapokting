<!-- page content -->
<div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Table Harga</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama Table</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $i=1;
                          foreach($listTable as $key){
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td>
                          <?php
                            $str = "";
                          if($key['namatable'] == 'detailKomoditi'){
                            $str = "Harga Komoditi";
                          }else if($key['namatable'] == 'hakAkses'){
                            $str = "Hak Akses";
                          }else if($key['namatable'] == 'kategoriKomoditi'){
                            $str = "Kategori Komoditi";
                          }else if($key['namatable'] == 'subDetailKomoditi'){
                            $str = "Sub Detail Komoditi";
                          }else{
                            $str = $key['namatable'];
                          }

                          echo ucfirst($str);
                          
                          ?>
                          </td>
                          <td><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/ExportData/getData/'.$key['namatable']; ?>"><i class="fa fa-download"></i> Export</a></td>
                        </tr>
                        <?php
                          }
                        ?>
                      </tbody>
                    </table>
					
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->