<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Table Harga</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a href="<?php echo base_url(); ?>district/HargaKomoditi/tambahHargaKomoditi" class="close-link badge bg-green"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <form class="form-horizontal form-label-left input_mask form-search-content" action="<?php echo base_url().$act; ?>" method="POST">

                      <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                        <label>Pasar</label>
                        <select class="form-control" name="pasar">
                            <option value="ALL">-- Semua Pasar --</option>
                            <?php
                              foreach($listpasar as $key){
                              $selected = "";
                              if($key['idPasar'] == $search['pasar']){
                                $selected = 'selected';
                              }
                            ?>
                              <option <?php echo $selected; ?> value="<?php echo $key['idPasar']; ?>"><?php echo $key['namaKecamatan'].' - '.$key['namaPasar']; ?></option>
                            <?php
                              }
                            ?>
                          </select>
                      </div>

                      <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                        <label>Komoditi</label>
                        <select class="form-control" name="komoditi">
                            <option value="ALL">-- Semua Komoditi --</option>
                            <?php
                              foreach($listKomoditi as $key){
                                $selected = "";
                              if($key['idKomoditi'] == $search['komoditi']){
                                $selected = 'selected';
                              }
                            ?>
                              <option <?php echo $selected; ?> value="<?php echo $key['idKomoditi']; ?>"><?php echo $key['namaKomoditi']; ?></option>
                            <?php
                              }
                            ?>
                          </select>
                      </div>

                      <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                        <label>Tanggal</label>
                        <input type="text" class="form-control has-feedback-left" id="single_cal1" placeholder="First Name" aria-describedby="inputSuccess2Status" name="tanggalharga" value="<?php echo !empty($search['tanggal'])?$search['tanggal']:''; ?>">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                      </div>

                      <div class="col-md-3 col-sm-6 col-xs-12 form-group has-feedback">
                        <label>&nbsp;</label>
                        <button type="submit" class="btn btn-success">Search</button>
                      </div>
                    </form>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Kecamatan</th>
                          <th>Pasar</th>
                          <th>Komoditi</th>
                          <th>Satuan</th>
                          <th>Tanggal</th>
                          <th>Harga</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $i=1;
                          foreach($list as $key){
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $key['namaKecamatan']; ?></td>
                          <td><?php echo $key['namaPasar']; ?></td>
                          <td><?php echo $key['labelkomoditi']; ?></td>
                          <td><?php echo $key['namaSatuan']; ?></td>
                          <td><?php echo longdate_indo($key['tanggalHarga']); ?></td>
                          <td><?php echo nominal($key['harga']); ?></td>
                          <td><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/HargaKomoditi/editHargaKomoditi/'.$key['idDetailKomoditi']; ?>"><i class="fa fa-download"></i> Ubah</a><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/HargaKomoditi/deleteHargaKomoditi/'.$key['idDetailKomoditi']; ?>"><i class="fa fa-download"></i> Hapus</a></td>
                        </tr>
                        <?php
                          }
                        ?>
                      </tbody>
                    </table>
					
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->