<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
<form class="form-horizontal form-label-left formHargaKomoditi" action="<?php echo base_url().$act; ?>" method="post">
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <span class="notifWarningKomoditi"></span>
                <span class="notifWarningHarga"></span>
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Lokasi Pasar</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><button type="button" id="backButtonNew" class="btn btn-primary" red-href="<?php echo base_url().'district/HargaKomoditi'; ?>">Cancel</button></li>
                      <li><button type="reset" class="btn btn-primary">Reset</button></li>
                      <li><button type="submit" class="btn btn-success">Submit</button></li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content well">
                    <br />
                    <div class="form-group">
                    <input type="hidden" class="form-control detailid" name="idDetail" value="<?php echo !empty($detailharga->idDetailKomoditi)?$detailharga->idDetailKomoditi:''; ?>">
                    <input type="hidden" class="form-control komoditiidsaved" value="<?php echo !empty($detailharga->subDetailIdSubDetail)?$detailharga->subDetailIdSubDetail:''; ?>">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Pasar</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="form-control pasarfield" name="pasar">
                            <option value="">-- PILIH --</option>
                            <?php
                              foreach($listpasar as $key){
                                $selected = "";

                              if(!empty($detailharga)){
                              	if($key['idPasar'] == $detailharga->pasarIdPasar){
                                  $selected = "selected";
                                }else{
                                  $selected = "";
                                }
                              }
                            ?>
                              <option <?php echo $selected; ?> value="<?php echo $key['idPasar']; ?>"><?php echo $key['namaKecamatan'].' - '.$key['namaPasar']; ?></option>
                            <?php
                              }
                            ?>
                          </select>
                          <span class="notifpasar notifwarning"></span>
                        </div>
                      </div>
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Tanggal</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control has-feedback-left tanggalInputHarga" id="single_cal1" placeholder="First Name" aria-describedby="inputSuccess2Status" name="tanggalharga" value="<?php echo !empty($detailharga->tanggalHarga)?$detailharga->tanggalHarga:''; ?>">
                                <span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span>
                        </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="row originalForm">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    <?php
                    if (empty($detailharga->idDetailKomoditi)){
                      ?>
                        <ul class="nav navbar-right panel_toolbox panel-custom-pasar">
                          <li><a class="addFieldNew"><i class="fa fa-plus"></i></a></li>
                        </ul>
                      <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Komoditi</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="form-control komoditispin" disabled name="komoditi[]">
                            
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Harga <span class="required">*</span></label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control validateHarga" disabled placeholder="Harga" name="harga[]" value="<?php echo !empty($detailharga->harga)?$detailharga->harga:''; ?>">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                  </div>
                </div>
              </div>
            </div>

            <span id="addedForm">
              
            </span>
          </form>
          </div>
        </div>
        <!-- /page content -->