<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>User</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <?php
                      if(in_array($this->session->userdata('idHakAkses'),array(1,2))){
                    ?>
                      <li><a href="<?php echo base_url(); ?>district/Member/tambahMember" class="close-link badge bg-green"><i class="fa fa-plus"></i></a>
                      </li>
                      <?php
                      }
                      ?>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <table id="datatable" class="table table-striped table-bordered">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Nama User</th>
                          <th>Level User</th>
                          <th>Action</th>
                        </tr>
                      </thead>


                      <tbody>
                      <?php
                      $no=1;
                        for($i=0;$i<count($list);$i++){
                          ?>
                          <tr>
                            <td><?php echo $no++; ?></td>
                            <td><?php echo $list[$i]['namaMember']; ?></td>
                            <td><?php echo $list[$i]['namaHakAkses']; ?></td>
                            <td><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/Member/editMember/'.$list[$i]['idMember']; ?>"><i class="fa fa-pencil"></i> Ubah</a>
                            <?php
                              if(in_array($this->session->userdata('idHakAkses'),array(1,2))){
                                if($this->session->userdata('user_id')!=$list[$i]['idMember']){
                            ?>
                            <a class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#confirm-delete" href="#" data-href="<?php echo base_url().'district/Member/deleteMember/'.$list[$i]['idMember']; ?>"><i class="fa fa-eraser"></i> Hapus</a>
                            <?php
                              }
                            }
                              ?></td>
                          </tr>
                          <?php
                        }
                      ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>