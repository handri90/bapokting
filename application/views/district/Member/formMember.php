<!-- page content -->
<div class="right_col" role="main">
          <div class="">

            <div class="row">

              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title; ?></h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form enctype="multipart/form-data" class="form-horizontal form-label-left" method="post" action="<?php echo base_url().$act; ?>">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Lengkap</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="hidden" name="iduser" class="form-control" value="<?php echo !empty($list->idMember)?$list->idMember:""; ?>">
                          <input required type="text" name="namalengkap" class="form-control" placeholder="Nama Lengkap" value="<?php echo !empty($list->namaMember)?$list->namaMember:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Username</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input required type="text" name="username" class="form-control" placeholder="Username" value="<?php echo !empty($list->username)?$list->username:""; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Password</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <?php
                            $req = "";
                            if(empty($list->idMember)){
                                $req = "required";
                            }
                            ?>
                          <input <?php echo $req; ?> type="password" name="passworduser" class="form-control" placeholder="Password">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Level User</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select name="leveluser" class="form-control">
                            <?php
                            for($i=0;$i<count($leveluser);$i++){
                                $select ="";
                                if($leveluser[$i]['idHakAkses']==$list->hakAksesIdHakAkses){
                                    $select = "selected";
                                }

                                ?>
                                <option <?php echo $select; ?> value="<?php echo $leveluser[$i]['idHakAkses']; ?>"><?php echo $leveluser[$i]['namaHakAkses']; ?></option>
                                <?php
                            }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Foto</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <label class="btn btn-primary btn-upload btnUpgrade" title="Upload file">
                          <?php
                          $req="";
                          if(empty($list->idMember)){
                            $req = "required";
                          }
                          ?>
                          <input <?php echo $req; ?> type="file" class="sr-only inputFotoUser" name="userfile" accept=".png,.jpeg,.jpg">
                          <span class="docs-tooltip" data-toggle="tooltip" title="Import image with Blob URLs">
                            <span class="fa fa-upload"></span>
                          </span>
                        </label>
                        <span class="outpulistfoto">
                          <?php
                          if(!empty($list->filename)){
                            ?>
                            <div style="margin-top:10px;padding:5px;"><img src="<?php echo base_url().'upload/fotouser/'.$list->filename; ?>" width="350" /></div>
                            <?php
                          }
                          ?>
                        </span>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" class="btn btn-primary" id="backButton">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>


            
          </div>
        </div>
        <!-- /page content -->