<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url().$act; ?>" method="post">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kategori Komoditi</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="hidden" class="form-control" name="idkomoditi" value="<?php echo !empty($list->idKomoditi)?$list->idKomoditi:''; ?>">
                          <select class="form-control" name="idKategoriKomoditi">
                          <?php
                            foreach($listkategori as $key){
                              $selected = "";
                              if($key['idKategoriKomoditi'] == $list->kategoriIdKategoriKomoditi){
                                $selected = "selected";
                              }
                          ?>
                            <option <?php echo $selected; ?> value="<?php echo $key['idKategoriKomoditi']; ?>"><?php echo $key['namaKategoriKomoditi']; ?></option>
                          <?php
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Komoditi</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="Nama Komoditi" name="namakomoditi" value="<?php echo !empty($list->namaKomoditi)?$list->namaKomoditi:''; ?>">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" id="backButton" class="btn btn-primary">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->