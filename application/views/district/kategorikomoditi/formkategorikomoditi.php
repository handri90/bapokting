<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url().$act; ?>" method="post">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Kategori Komoditi</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="hidden" class="form-control" name="idkategori" value="<?php echo !empty($list->idKategoriKomoditi)?$list->idKategoriKomoditi:''; ?>">
                          <input type="text" class="form-control" placeholder="Nama Kategori Komoditi" name="namakategorikomoditi" value="<?php echo !empty($list->namaKategoriKomoditi)?$list->namaKategoriKomoditi:''; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Urutan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="Urutan" name="urutan" value="<?php echo !empty($list->orderByKategoriKomoditi)?$list->orderByKategoriKomoditi:''; ?>">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" id="backButton" class="btn btn-primary">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->