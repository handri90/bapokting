<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url().$act; ?>" method="post">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Komoditi</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="hidden" class="form-control" name="idsubkomoditi" value="<?php echo !empty($list->idSubDetailKomoditi)?$list->idSubDetailKomoditi:''; ?>">
                          <select class="form-control" name="idkomoditi">
                          <?php
                              foreach($listkomoditi as $key){
                                $explNamaKomoditi = explode('|', $key['namakomoditi']);
                                $explIdKomoditi = explode('|', $key['idkomoditi']);
                            ?>
                              <optgroup label="<?php echo $key['namaKategoriKomoditi']; ?>">
                                <?php
                                for($i=0;$i<count($explNamaKomoditi);$i++){
                                  $selected = "";
                                  if($explIdKomoditi[$i] == $list->komoditiIdKomoditi){
                                    $selected = "selected";
                                  }
                                  ?>
                                  <option <?php echo $selected; ?> value="<?php echo $explIdKomoditi[$i]; ?>"><?php echo $explNamaKomoditi[$i]; ?></option>
                                  <?php
                                }
                                ?>
                              </optgroup>
                            <?php
                              }
                            ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Barang</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="Nama Barang" name="namabarang" value="<?php echo !empty($list->namaSubDetailKomoditi)?$list->namaSubDetailKomoditi:''; ?>">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Satuan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <select class="form-control" name="satuan">
                          <?php
                            foreach($listsatuan as $key){
                              $selected = "";
                              if($key['idSatuan'] == $list->satuanIdSatuan){
                                $selected = "selected";
                              }
                          ?>
                            <option <?php echo $selected; ?> value="<?php echo $key['idSatuan']; ?>"><?php echo $key['namaSatuan']; ?></option>
                          <?php
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" id="backButton" class="btn btn-primary">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->