<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Table Sub Detail Komoditi</h2>
                    <ul class="nav navbar-right panel_toolbox">
                      <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                      </li>
                      <li><a href="<?php echo base_url(); ?>district/SubDetailKomoditi/tambahSubDetailKomoditi" class="close-link badge bg-green"><i class="fa fa-plus"></i></a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                  <form class="form-horizontal form-label-left input_mask form-search-content" action="<?php echo base_url().$act; ?>" method="POST">

                  <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label>Kategori Komoditi</label>
                    <select class="form-control kategorisearch" name="kategori">
                        <option value="ALL">-- Semua Kategori --</option>
                        <?php
                          foreach($listkategori as $key){
                          $selected = "";
                          if($key['idKategoriKomoditi'] == $search['kategori']){
                            $selected = 'selected';
                          }
                        ?>
                          <option <?php echo $selected; ?> value="<?php echo $key['idKategoriKomoditi']; ?>"><?php echo $key['namaKategoriKomoditi']; ?></option>
                        <?php
                          }
                        ?>
                      </select>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label>Komoditi</label>
                    <select class="form-control komoditassearch" name="komoditas">
                        <?php
                        if($search['kategori'] != 'ALL'){
                          if(!empty($listkomoditi)){
                            ?>
                            <option value="ALL">-- Semua Kategori --</option>
                            <?php
                            foreach($listkomoditi as $key){
                              $selected = "";
                              if($key['idKomoditi'] == $search['komoditas']){
                                $selected = 'selected';
                              }
                            ?>
                              <option <?php echo $selected; ?> value="<?php echo $key['idKomoditi']; ?>"><?php echo $key['namaKomoditi']; ?></option>
                            <?php
                              }
                          }
                        }
                        ?>
                      </select>
                  </div>
                  <div class="col-md-4 col-sm-6 col-xs-12 form-group has-feedback">
                    <label>&nbsp;</label>
                    <button type="submit" class="btn btn-success">Search</button>
                  </div>
                  </form>
                    <table id="datatable-responsive" class="table table-striped table-bordered dt-responsive nowrap" cellspacing="0" width="100%">
                      <thead>
                        <tr>
                          <th>No</th>
                          <th>Komoditi</th>
                          <th>Satuan</th>
                          <th>Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                          $i=1;
                          foreach($list as $key){
                        ?>
                        <tr>
                          <td><?php echo $i++; ?></td>
                          <td><?php echo $key['namasubdetailkomoditi']; ?></td>
                          <td><?php echo $key['namaSatuan']; ?></td>
                          <td><a class="btn btn-primary pull-right" style="margin-right: 5px;" href="<?php echo base_url().'district/SubDetailKomoditi/editsubkomoditi/'.$key['idSubDetailKomoditi']; ?>"><i class="fa fa-download"></i> Ubah</a>
                          <a class="btn btn-primary pull-right" style="margin-right: 5px;" data-toggle="modal" data-target="#confirm-delete" href="#" data-href="<?php echo base_url().'district/SubDetailKomoditi/deleteSubKomoditi/'.$key['idSubDetailKomoditi']; ?>"><i class="fa fa-eraser"></i> Hapus</a>
                          </td>
                        </tr>
                        <?php
                          }
                        ?>
                      </tbody>
                    </table>
					
					
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->