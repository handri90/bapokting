<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $title2; ?></h2>
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                    <br />
                    <form class="form-horizontal form-label-left" action="<?php echo base_url().$act; ?>" method="post">
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Kecamatan</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                        <input type="hidden" class="form-control" name="idpasar" value="<?php echo !empty($list->idPasar)?$list->idPasar:''; ?>">
                          <select class="form-control" name="kecamatan">
                          <?php
                            foreach($listkecamatan as $key){
                              $selected = "";
                              if($key['idKecamatan'] == $list->kecamatanIdKecamatan){
                                $selected = "selected";
                              }
                          ?>
                            <option <?php echo $selected; ?> value="<?php echo $key['idKecamatan']; ?>"><?php echo $key['namaKecamatan']; ?></option>
                          <?php
                            }
                          ?>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-3 col-sm-3 col-xs-12">Nama Pasar</label>
                        <div class="col-md-9 col-sm-9 col-xs-12">
                          <input type="text" class="form-control" placeholder="Nama Pasar" name="namapasar" value="<?php echo !empty($list->namaPasar)?$list->namaPasar:''; ?>">
                        </div>
                      </div>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                          <button type="button" id="backButton" class="btn btn-primary">Cancel</button>
                          <button type="reset" class="btn btn-primary">Reset</button>
                          <button type="submit" class="btn btn-success">Submit</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->